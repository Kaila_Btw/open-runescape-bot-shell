import React from "react";
import {IRegistry} from "../hooks/useRegistry";

export const RegistryContext = React.createContext<IRegistry>({
    subjects: [],
    unregisterPlayer: () => {
    },
    groups: [],
    players: [],
    save: () => {
    }
});