/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.31.861 on 2021-06-26 17:45:43.

export interface AppearanceDTO {
    wornItemAppearanceIds: number[];
    head: number;
    body: number;
    hair: number;
    top: number;
    legs: number;
    skin: number;
}

export interface LabeledNumberDTO {
    name: string;
    level: number;
}

export interface ManagedPlayerDTO {
    username: string;
    status: string;
    location: string;
    area: string;
    appearance: AppearanceDTO;
    messageHistory: string[];
    inventory: { [index: string]: number };
    connectionState: ConnectionStateDTO;
    reconnectTimeTotal: number;
    reconnectTimeRemaining: number;
    scriptElapsedTime: number;
    scriptState: ScriptState;
    errorMessage: string;
}

export interface PlayerLoginRequestDTO {
    username: string;
    password: string;
}

export interface ScriptDefinitionDTO<T> {
    name: string;
    defaultParameters: T;
    options: ScriptParam[];
}

export interface ScriptLoadRequestDTO {
    name: string;
    parameters: any;
}

export interface SkillsDTO {
    skills: LabeledNumberDTO[];
}

export interface StatDTO {
    name: string;
    currentLvl: number;
    maxLvl: number;
}

export interface StatsDTO {
    stats: StatDTO[];
}

export interface GroupDefinition {
    name: string;
    players: PlayerDefinition[];
}

export interface GroupDefinitionBuilder {
}

export interface PlayerDefinition {
    username: string;
    password: string;
    active: boolean;
}

export interface PlayerDefinitionBuilder {
}

export interface BooleanParam extends ScriptParam {
}

export interface BooleanParamBuilder<C, B> extends ScriptParamBuilder<C, B> {
}

export interface BooleanParamBuilderImpl extends BooleanParamBuilder<BooleanParam, BooleanParamBuilderImpl> {
}

export interface EnabledCondition {
    key: string;
    value: any;
}

export interface EnabledConditionBuilder {
}

export interface EnumParam<T> extends ScriptParam {
    values: SelectableValue<T>[];
}

export interface EnumParamBuilder<T, C, B> extends ScriptParamBuilder<C, B> {
}

export interface EnumParamBuilderImpl<T> extends EnumParamBuilder<T, EnumParam<T>, EnumParamBuilderImpl<T>> {
}

export interface InstructionsParam extends ScriptParam {
}

export interface InstructionsParamBuilder<C, B> extends ScriptParamBuilder<C, B> {
}

export interface InstructionsParamBuilderImpl extends InstructionsParamBuilder<InstructionsParam, InstructionsParamBuilderImpl> {
}

export interface Itemable {
    itemId: number;
}

export interface NumberParam extends ScriptParam {
}

export interface NumberParamBuilder<C, B> extends ScriptParamBuilder<C, B> {
}

export interface NumberParamBuilderImpl extends NumberParamBuilder<NumberParam, NumberParamBuilderImpl> {
}

export interface ScriptParam {
    key: string;
    label: string;
    multiple: boolean;
    enabledConditions: EnabledCondition[];
    type: ScriptParamType;
}

export interface ScriptParamBuilder<C, B> {
}

export interface SelectableItem<L> extends SelectableValue<L> {
    itemId: number;
}

export interface SelectableValue<L> {
    label: string;
    value: L;
}

export interface StringParam extends ScriptParam {
}

export interface StringParamBuilder<C, B> extends ScriptParamBuilder<C, B> {
}

export interface StringParamBuilderImpl extends StringParamBuilder<StringParam, StringParamBuilderImpl> {
}

export enum ConnectionStateDTO {
    FATAL_ERROR = "FATAL_ERROR",
    RECONNECTING = "RECONNECTING",
    LOGGING_IN = "LOGGING_IN",
    LOGGED_IN = "LOGGED_IN",
}

export enum ScriptParamType {
    STRING = "STRING",
    NUMBER = "NUMBER",
    ENUM = "ENUM",
    INSTRUCTIONS = "INSTRUCTIONS",
    BOOLEAN = "BOOLEAN",
}

export enum ScriptState {
    RUNNING = "RUNNING",
    PAUSED = "PAUSED",
    SLEEPING = "SLEEPING",
    SHUTTING_DOWN = "SHUTTING_DOWN",
    FINISHED = "FINISHED",
    KILLED = "KILLED",
}
