import {axios} from "../util/axios";

export function getScripts() {
    return axios.get("scripts")
        .then(result => result.data)
        .catch(err => console.error(err));
}