import {axios} from "../util/axios";
import {GroupDefinition, PlayerDefinition} from "../importedTypes";

const NAMESPACE = "/registry";

export function getAllGroups(): Promise<GroupDefinition[]> {
    return axios.get(`${NAMESPACE}/group`)
        .then(result => result.data)
        .catch(console.error)
}

export function getAllPlayers(): Promise<PlayerDefinition[]> {
    return axios.get(`${NAMESPACE}/player`)
        .then(result => result.data)
        .catch(console.error)
}

export function savePlayer(player: PlayerDefinition): Promise<PlayerDefinition> {
    return axios.post(`${NAMESPACE}/player`, {...player})
        .then(result => result.data)
        .catch(console.error)
}

export function deletePlayer(username: string): Promise<any> {
    return axios.get(`${NAMESPACE}/player/${username}/delete`)
        .then(result => result.data)
        .catch(console.error)
}