import {axios} from "../util/axios";
import {ScriptLoadRequestDTO, StatsDTO} from "../importedTypes";

const NAMESPACE = "/player";

export function loadScript(username: string, request: ScriptLoadRequestDTO) {
    return axios.post(`${NAMESPACE}/${username}/script/load`, request)
        .then(result => result.data)
        .catch(console.error)
}

export function startScript(username: string) {
    return axios.get(`${NAMESPACE}/${username}/script/start`)
        .then(result => result.data)
        .catch(console.error)
}

export function pauseScript(username: string) {
    return axios.get(`${NAMESPACE}/${username}/script/pause`)
        .then(result => result.data)
        .catch(console.error)
}

export function stopScript(username: string) {
    return axios.get(`${NAMESPACE}/${username}/script/stop`)
        .then(result => result.data)
        .catch(console.error)
}

export function getStats(username: string): Promise<StatsDTO> {
    return axios.get(`${NAMESPACE}/${username}/stats`)
        .then(result => result.data)
        .catch(console.error) as Promise<StatsDTO>
}

export function login(username: string): Promise<any> {
    return axios.get(`${NAMESPACE}/${username}/login`)
        .then(result => result.data)
        .catch(console.error) as Promise<boolean>
}

export function logout(username: string): Promise<boolean> {
    return axios.get(`${NAMESPACE}/${username}/logout`)
        .then(result => result.data)
        .catch(console.error) as Promise<boolean>
}

export function searchPlayers(filterQuery: string): Promise<string[]> {
    return axios.get(`${NAMESPACE}`, {
        params: {
            query: filterQuery
        }
    })
        .then(result => result.data)
        .catch(console.error) as Promise<string[]>
}