import Stomp, {Client} from "stompjs";
import SockJS from "sockjs-client";

const sockjs_url = "http://localhost:8080/gs-guide-websocket";
const socket = new SockJS(sockjs_url);
// const websocket = new WebSocket(sockjs_url);
const stompClient = Stomp.over(socket as any);
const getPromise = () => new Promise<Client>((resolve, reject) => {
    // @ts-ignore
    stompClient.debug = null;
    stompClient.connect(
        {},
        frame => {
            console.log(`Connected to websocket: ${frame}`);
            resolve(stompClient);
        },
        error => {
            console.log(error);
            reject(error);
        })
});
let stompPromise = getPromise();

setInterval(() => {
    getStompClient().then(stompClient => {
        if (!stompClient.connected) {
            stompPromise = getPromise();
        }
    })
}, 1000)

export async function getStompClient() {
    return await stompPromise;
}