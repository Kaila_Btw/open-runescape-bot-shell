import React, {useEffect, useState} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {Popover, Table, TableBody, TableCell, TableContainer, TableRow} from "@material-ui/core";
import {getStats} from "../service/player-service";
import {StatDTO} from "../importedTypes";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            position: 'absolute',
            width: 130,
            backgroundColor: theme.palette.background.paper,
            color: theme.palette.text.primary,
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
        modalCenter: {
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)"
        },
        modalActions: {
            marginTop: theme.spacing(1),
            textAlign: "right"
        },
        table: {
            minWidth: "130px",
        },
        tableCell: {
            padding: "3px",
            border: "none"
        }
    }),
);

interface StatsModalProps {
    isOpen: boolean,
    onClose: () => void,
    anchorEl: null | Element,
    username: string
}

export function StatsPopover(props: StatsModalProps) {
    const classes = useStyles();
    const {isOpen, onClose, anchorEl, username} = props;
    const [stats, setStats] = useState<any>([]);

    useEffect(() => {
        getStats(username)
            .then(statsDTO => setStats(statsDTO.stats))
            .catch(console.error)
    }, [username]);

    return (
        <Popover
            anchorEl={anchorEl}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
            }}
            classes={{
                paper: classes.paper,
            }}
            open={isOpen}
            onClose={onClose}
        >
            <TableContainer component={"div"}>
                <Table className={classes.table} aria-label="stats table">
                    <TableBody>
                        {stats.map((stat: StatDTO) => (
                            <TableRow key={stat.name}>
                                <TableCell className={classes.tableCell} component="th" scope="row">
                                    {stat.name}
                                </TableCell>
                                <TableCell className={classes.tableCell}
                                           align="right">
                                    {stat.currentLvl}/{stat.maxLvl}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Popover>
    );
}