import "./item-sprite.css";

export function itemUrlFromId(id: string) {
    return process.env.PUBLIC_URL + `/res/items/${id}.png`;
}

export function ItemSprite({id, quantity}) {
    return <div className={"item-sprite"}>
        <img className={"item-sprite-img"} src={itemUrlFromId(id)} alt={id}/>
        <span className={"item-sprite-quantity"}>{"x" + quantity}</span>
    </div>;
}