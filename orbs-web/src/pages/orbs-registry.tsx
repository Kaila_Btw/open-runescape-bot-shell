import {PlayerTable} from "../components/player-table/player-table";
import {makeStyles} from "@material-ui/core/styles";
import {Paper} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    registry: {
        padding: theme.spacing(2),
        display: "flex",
        flexFlow: "row wrap",
        alignItems: "stretch"
    },
    tableContainer: {
        padding: theme.spacing(2),
        boxSizing: "border-box",
        margin: theme.spacing(2),
        flexGrow: 1,
        maxWidth: 500,
    }
}))

export function ORBSRegistry() {
    const styles = useStyles();
    return <div className={styles.registry}>
        <Paper className={styles.tableContainer}>
            <PlayerTable/>
        </Paper>
        {/*<Paper className={styles.tableContainer}>*/}
        {/*    <PlayerTable/>*/}
        {/*</Paper>*/}
    </div>;
}