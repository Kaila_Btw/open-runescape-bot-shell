import React, {useEffect} from "react";
import {player as generatePlayer} from "rsc-sprite-generator";
import {AppearanceDTO} from "../importedTypes";

interface PlayerSpriteProps {
    appearance: AppearanceDTO
}

function _PlayerSprite(props: PlayerSpriteProps) {
    const {appearance} = props;

    const {body, head, hair, top, legs, skin} = appearance;
    useEffect(() => {
        getPlayer(appearance).then(avatar => {
            const parentElement = document.getElementById(getIdKey(appearance));
            if (parentElement) {
                parentElement.innerHTML = "";
                parentElement.appendChild(avatar);
            }
        });
    }, [body, head, hair, top, legs, skin]);

    return <div id={getIdKey(appearance)} className={"player-sprite"}/>;
}

const getPlayer = async (appearance: AppearanceDTO) => {
    const HEADS = [0, 3, 5, 6, 7];
    const {head, body, hair, top, legs, skin} = appearance;
    try {
        return await generatePlayer({
            angle: 1,
            head: HEADS[head - 1],
            body: body - 1,
            colours: {
                hair: hair - 1,
                top: top - 1,
                legs: legs - 1,
                skin: skin - 1
            },
            wielding: []
        });
    } catch (error) {
        console.error(error);
    }
}

export const PlayerSprite = React.memo(_PlayerSprite);

function getIdKey(appearance: AppearanceDTO) {
    const {body, head, hair, top, legs, skin} = appearance;
    return `${body}-${head}-${hair}-${top}-${legs}-${skin}-spritePreview`;
}