import React, {useEffect, useState} from "react";
import {PlayerSprite} from "./player-sprite";
import {PlayerMessageHistory} from "./player-message-history";
import {PlayerInventory} from "./player-inventory";
import {PlayerMenu} from "./player-menu";
import {Box, CircularProgress, createStyles, Paper, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {ConnectionStateDTO, ManagedPlayerDTO} from "../importedTypes";
import CloseIcon from '@material-ui/icons/Close';
import {logout} from "../service/player-service";
import {ActionButton} from "../components/action-button/action-button";

interface PlayerInfoProps {
    player: ManagedPlayerDTO,
    id: string
}

const useStyles = makeStyles(theme =>
    createStyles({
        outerContainer: {
            margin: theme.spacing(1),
            padding: theme.spacing(1),
            width: "300px",
            position: "relative",
            opacity: "0%",
            display: "flex",
            flexDirection: "column"
        },
        playerContainer: {
            display: "flex",
            flexDirection: "row"
        },
        playerControls: {
            flexGrow: 1,
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-end"
        },
        reconnectingContainer: {
            minHeight: "200px",
            height: "100%",
            width: "100%",
            display: "flex",
            justifyContent: "flex-start",
            alignContent: "center",
            flexDirection: "column",
            "& > *": {
                margin: "auto"
            }
        }
    })
)

export function Player(props: PlayerInfoProps) {
    const styles = useStyles();
    const {player} = props;
    const {
        username,
        appearance,
        location,
        area,
        status,
        messageHistory,
        inventory,
        reconnectTimeTotal,
        reconnectTimeRemaining,
        connectionState,
        errorMessage
    } = player;

    let [timeElapsed, setTimeElapsed] = useState(0);

    useEffect(() => {
        const timer = setInterval(() => {
            setTimeElapsed(timeElapsed => timeElapsed + 100);
        }, 100)
        return () => clearInterval(timer);
    }, [connectionState]);

    useEffect(() => {
        setTimeElapsed(reconnectTimeTotal - reconnectTimeRemaining);
    }, [reconnectTimeRemaining])

    const getReconnectionText = () => {
        if (!countingDown()) {
            return "";
        }
        if (player.connectionState !== ConnectionStateDTO.FATAL_ERROR) {
            return "Retry in";
        }
        return "Removing in";
    }

    const countingDown = () => {
        return timeElapsed / reconnectTimeTotal < 1;
    }

    return (
        <Paper className={styles.outerContainer} id={props.id}>
            {
                connectionState === ConnectionStateDTO.LOGGED_IN &&
                <>
                    <div className={styles.playerContainer}>
                        <Box position="absolute" top={10} right={10}>
                            <ActionButton
                                onComplete={() => logout(username)}
                                label={"Logout"}
                                icon={<CloseIcon color={"error"}/>}
                            />
                        </Box>
                        <PlayerSprite appearance={appearance}/>
                        <div className={"player-info-body"}>
                            <span className={"player-title"}>
                                <span className={"player-name"}>{username}</span>
                                <span className={"player-location"}>{area}{location}</span>
                                <span className={"player-status"}>{status || "Idle"}</span>
                            </span>
                            <PlayerInventory inventory={inventory}/>
                        </div>
                    </div>
                    <div className={styles.playerControls}>
                        <PlayerMenu player={player}/>
                        <PlayerMessageHistory messageHistory={messageHistory}/>
                    </div>
                </>
            }
            {
                connectionState !== ConnectionStateDTO.LOGGED_IN &&
                <>
                    <div className={styles.reconnectingContainer}>
                        <Box position="absolute" top={10} right={10}>
                            <ActionButton
                                onComplete={() => logout(username)}
                                label={"Logout"}
                                icon={<CloseIcon color={"error"}/>}
                            />
                        </Box>
                        <Typography variant={"h5"}>{username}</Typography>
                        {
                            connectionState === ConnectionStateDTO.FATAL_ERROR &&
                            <div style={{textAlign: "center"}}>
                                <Typography variant={"h6"}>Fatal Error</Typography>
                                <Typography variant={"caption"}>{errorMessage}</Typography>
                            </div>
                        }
                        {
                            (connectionState === ConnectionStateDTO.LOGGING_IN ||
                                connectionState === ConnectionStateDTO.RECONNECTING) &&
                            <>
                                <div style={{textAlign: "center"}}>
                                    <Typography variant={"h6"}>Reconnecting</Typography>
                                    <Typography variant={"caption"}>{errorMessage}</Typography>
                                </div>

                            </>
                        }
                        <Box position="relative" display="inline-flex">
                            {
                                countingDown() &&
                                <CircularProgress
                                    thickness={1}
                                    size={100}
                                    variant="determinate"
                                    value={(timeElapsed) / reconnectTimeTotal * 100}
                                />
                            }
                            {
                                !countingDown() &&
                                <CircularProgress
                                    thickness={1}
                                    size={100}
                                />
                            }
                            <Box
                                top={0}
                                left={0}
                                bottom={0}
                                right={0}
                                position="absolute"
                                display="flex"
                                flexWrap="true"
                                alignItems="center"
                                justifyContent="center"
                                textAlign={"center"}>
                                <Typography variant="body1" component="div" color="textSecondary">
                                    <>
                                        <Typography
                                            variant="caption"
                                            component="span"
                                            color="textSecondary">{getReconnectionText()}</Typography>
                                        <br/>
                                        {
                                            countingDown() &&
                                            `${Math.round(reconnectTimeRemaining / 1000)}s`
                                        }
                                    </>
                                </Typography>
                            </Box>
                        </Box>
                    </div>
                </>
            }
        </Paper>
    )
}