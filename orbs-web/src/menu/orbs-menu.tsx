import {createStyles, Drawer, Hidden, List, ListItem, ListItemIcon, ListItemText, useTheme} from "@material-ui/core";
import React from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import DashboardIcon from '@material-ui/icons/Dashboard';
import GroupIcon from '@material-ui/icons/Group';
import {useHistory} from "react-router";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        drawer: {
            [theme.breakpoints.up('sm')]: {
                width: drawerWidth,
                flexShrink: 0,
            },
        },
        appBar: {
            [theme.breakpoints.up('sm')]: {
                width: `calc(100% - ${drawerWidth}px)`,
                marginLeft: drawerWidth,
            },
        },
        menuButton: {
            marginRight: theme.spacing(2),
            [theme.breakpoints.up('sm')]: {
                display: 'none',
            },
        },
        // necessary for content to be below app bar
        toolbar: theme.mixins.toolbar,
        drawerPaper: {
            width: drawerWidth,
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
        },
    }),
);

export function OrbsMenu() {
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const history = useHistory();

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div>
            <List>
                <ListItem button onClick={() => history.push("/dashboard")}>
                    <ListItemIcon>
                        <DashboardIcon/>
                    </ListItemIcon>
                    <ListItemText primary={"Dashboard"} color={"primary"}/>
                </ListItem>
                <ListItem button onClick={() => history.push("/registry")}>
                    <ListItemIcon>
                        <GroupIcon/>
                    </ListItemIcon>
                    <ListItemText primary={"Registry"}/>
                </ListItem>
                {/*<ListItem button onClick={() => history.push("/scripts")}>*/}
                {/*    <ListItemIcon>*/}
                {/*        <SubtitlesIcon/>*/}
                {/*    </ListItemIcon>*/}
                {/*    <ListItemText primary={"Scripts"}/>*/}
                {/*</ListItem>*/}
                {/*<ListItem button onClick={() => history.push("/support")}>*/}
                {/*    <ListItemIcon>*/}
                {/*        <HelpIcon/>*/}
                {/*    </ListItemIcon>*/}
                {/*    <ListItemText primary={"Support"}/>*/}
                {/*</ListItem>*/}
            </List>
        </div>
    );

    return <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
            <Drawer
                variant="temporary"
                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                open={mobileOpen}
                onClose={handleDrawerToggle}
                classes={{
                    paper: classes.drawerPaper,
                }}
                ModalProps={{
                    keepMounted: true, // Better open performance on mobile.
                }}
            >
                {drawer}
            </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
            <Drawer
                classes={{
                    paper: classes.drawerPaper,
                }}
                variant="permanent"
                open
            >
                {drawer}
            </Drawer>
        </Hidden>
    </nav>;
}