import {
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControlLabel,
    TextField
} from "@material-ui/core";
import React, {useState} from "react";

interface AddPlayerDialogProps {
    isOpen: boolean,
    onClose: (username?: string, password?: string, save?: boolean) => void,
}

export function AddPlayerDialog(props: AddPlayerDialogProps) {
    const {isOpen, onClose} = props;
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [save, setSave] = useState(true);
    return <Dialog open={isOpen} onClose={() => onClose()} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add Player</DialogTitle>
        <DialogContent>
            <TextField
                autoFocus
                id="username"
                label="Username"
                type="text"
                variant={"outlined"}
                onChange={evt => setUsername(evt.target.value)}
                value={username}
                style={{marginBottom: 10}}
                fullWidth
            />
            <TextField
                id="password"
                label="Password"
                type="password"
                variant={"outlined"}
                onChange={evt => setPassword(evt.target.value)}
                value={password}
                style={{marginBottom: 10}}
                fullWidth
            />
            <FormControlLabel
                control={
                    <Checkbox
                        onChange={evt => setSave(evt.target.checked)}
                        checked={save}
                        color={"primary"}
                    />
                }
                label={"Automatically Login on Startup"}
            />
        </DialogContent>
        <DialogActions>
            <Button onClick={() => onClose()} color="primary">
                Cancel
            </Button>
            <Button onClick={() => onClose(username, password, save)} color="primary">
                Add Player
            </Button>
        </DialogActions>
    </Dialog>
}