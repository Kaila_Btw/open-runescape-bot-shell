module.exports = {
    devServer: {
        open: true,
        historyApiFallback: true,
        proxy: {
                '/api': {
                        target: 'http://localhost:8080',
                        secure: false
                }
        }
    }
}
