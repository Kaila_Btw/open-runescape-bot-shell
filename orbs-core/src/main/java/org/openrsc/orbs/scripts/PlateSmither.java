package org.openrsc.orbs.scripts;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldNameConstants;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.model.Area;
import org.openrsc.orbs.net.connection.player.script.ItemIds;
import org.openrsc.orbs.net.connection.player.script.Locations;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.connection.player.script.model.BarType;

import java.util.Collections;
import java.util.Map;

@Log4j2
public class PlateSmither extends Script<PlateSmither.Params> {
    private final Area VARROCK_WEST_AREA = Area.fromPoint(Locations.VARROCK_WEST.getLocation(), 10);
    private final Integer ANVIL_ID = 50;
    private final ScriptAPI api;

    public PlateSmither(ScriptAPI api) {
        super(api);
        this.api = api;
    }

    @Override
    public void run() {
        api.sleepIfFatigueAbove(getParams().getFatigueLimit());
        if (!VARROCK_WEST_AREA.contains(api.getLocation())) {
            api.setStatus("Returning to Varrock West");
            api.walkTo(Locations.VARROCK_WEST.getLocation());
        } else {
            final Integer barId = getParams().getBarType().getItemId();
            if (api.getInventoryCount(ItemIds.HAMMER.id()) == 0) {
                api.setStatus("Obtaining Hammer");
                api.walkTo(Locations.VARROCK_WEST.getLocation());
                api.openBank();
                api.depositAllExcept(ItemIds.HAMMER.id(), ItemIds.SLEEPING_BAG.id());
                api.withdraw(ItemIds.HAMMER.id(), 1);
                if (api.getInventoryCount(ItemIds.HAMMER.id()) == 0) {
                    api.displayMessage("Unable to find hammer in bank or inventory. Stopping...");
                    api.shutdown();
                }
                api.closeBank();
            } else if (api.getInventoryCount(barId) < 5) {
                api.setStatus("Resupplying");
                api.walkTo(Locations.VARROCK_WEST.getLocation());
                api.openBank();
                api.depositAllExcept(ItemIds.HAMMER.id(), ItemIds.SLEEPING_BAG.id());
                api.withdraw(barId, 25);
                if (api.getInventoryCount(ItemIds.HAMMER.id()) == 0) {
                    api.withdraw(ItemIds.HAMMER.id(), 1);
                }
                api.closeBank();
            } else {
                api.walkTo(148, 512);
                api.findNearestObject(ANVIL_ID).ifPresent(anvil -> {
                    api.setStatus("Smithing Plates");
                    api.useItemOnObject(api.findItemInInventory(barId).orElse(0), anvil);
                    api.pauseUntil(api::inMenu, 3000);
                    if (api.inMenu()) {
                        // Make armour
                        api.chooseOption(1);
                        api.pauseUntil(api::inMenu, 3000);
                        if (api.inMenu()) {
                            // Armour
                            api.chooseOption(2);
                            api.pauseUntil(api::inMenu, 3000);
                            if (api.inMenu()) {
                                // Plate mail body
                                api.chooseOption(1);
                            }
                        }
                    }
                });
            }
        }
    }

    @Override
    public void shutdown() {
        api.walkTo(Locations.VARROCK_WEST.getLocation());
        api.openBank();
        api.depositAllExcept(ItemIds.SLEEPING_BAG.id());
        api.closeBank();
        done();
    }

    @Override
    public Map<String, String> getMetrics() {
        return Collections.emptyMap();
    }

    @Override
    public Params getDefaultParameters() {
        return Params.builder().build();
    }

    @Builder
    @Getter
    @FieldNameConstants
    public static class Params {
        @Builder.Default
        final int fatigueLimit = 95;
        @Builder.Default
        final BarType barType = BarType.STEEL;
    }
}
