package org.openrsc.orbs.scripts;

import com.google.common.eventbus.Subscribe;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.ObjectUtils;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.connection.player.script.ItemIds;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.connection.player.script.event.ScriptEvent;
import org.openrsc.orbs.net.connection.player.script.event.TradeRequest;
import org.openrsc.orbs.net.connection.player.script.model.Player;

public class ItemAcceptor extends Script<ObjectUtils.Null> {

    private boolean banking = false;
    private Player currentSupplier = null;

    public ItemAcceptor(ScriptAPI api) {
        super(api);
    }

    @Override
    public void run() {
        if (api.isTrading()) {
            api.acceptTrade();
        }
    }

    @Override
    public void shutdown() {
        api.walkToNearestBank();
        api.openBank();
        api.depositAllExcept(ItemIds.SLEEPING_BAG.id());
        api.closeBank();
        done();
    }

    @Override
    public ObjectUtils.Null getDefaultParameters() {
        return null;
    }


    @Subscribe
    public void onTradeRequest(TradeRequest request) {
        Player player = request.getPlayer();
        if (canTrade() && (currentSupplier == null || currentSupplier.equals(player))) {
            currentSupplier = player;
            api.broadcast(
                    FreeSpacesEvent.builder()
                            .targetPlayer(player.getUsername())
                            .freeSpaces(30 - api.getInventory().size())
                            .build()
            );
            api.sendTradeRequest(player.getPlayerId());
        }
    }

    private boolean canTrade() {
        return !api.isTrading() && !banking;
    }

    @Builder
    @Getter
    public static class FreeSpacesEvent extends ScriptEvent {
        private final String targetPlayer;
        private final int freeSpaces;
    }

    @Builder
    @Getter
    public static class MyLocationEvent extends ScriptEvent {
        private final String sender;
        private final Point location;
    }
}
