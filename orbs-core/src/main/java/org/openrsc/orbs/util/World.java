package org.openrsc.orbs.util;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.model.Direction;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.model.Point3D;
import org.openrsc.orbs.model.Tile;
import org.openrsc.orbs.model.TileTraversal;
import org.openrsc.orbs.model.TileValue;
import org.openrsc.orbs.model.data.DoorDef;
import org.openrsc.orbs.model.data.Sector;
import org.openrsc.orbs.model.data.TileDef;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.openrsc.orbs.model.Direction.*;

@Log4j2
@Component
public class World {
    public static final int DISTANCE_BETWEEN_FLOORS = 944;
    public static final int REGION_SIZE = 48;
    public static final int MAX_WORLD_HEIGHT = 4032; // 3776
    public static final int MAX_WORLD_WIDTH = 1008; // 944
    private static final int[] ALLOWED_WALL_ID_TYPES = {5, 6, 14, 42, 63, 128, 229, 230};
    private final Map<Point, TileValue> rscCoordTileValue = new HashMap<>(20000, .5f);
    private final Map<Point, TileTraversal> rscCoordTileTraversal = new HashMap<>(20000, .5f);
    private final Map<Point3D, Sector> sectors = new TreeMap<>(Comparator.naturalOrder());
    private final Map<Point3D, byte[]> sectorData = new TreeMap<>(Comparator.naturalOrder());
    private final ResourceLoader resourceLoader;
    long unpackRegionTotal = 0;
    long initializeSectorTotal = 0;

    public World(ResourceLoader resourceLoader) throws IOException {
        this.resourceLoader = resourceLoader;
        long start = System.currentTimeMillis();
        collectSectors();
        long end = System.currentTimeMillis();
        log.info("Sectors loaded in " + (end - start) + " ms");

        initializeSectors();
        log.info(MessageFormat.format("unpackRegion runtime {0} ms", unpackRegionTotal));
        log.info(MessageFormat.format("initializeSector runtime {0} ms", initializeSectorTotal));
    }

    private static boolean projectileClipAllowed(final int wallID) {
        for (final int allowedWallIdType : ALLOWED_WALL_ID_TYPES) {
            if (allowedWallIdType == wallID) {
                return true;
            }
        }
        return false;
    }

    private void collectSectors() throws IOException {
        int wildX = 2304;
        for (int lvl = 0; lvl < 4; lvl++) {
            int wildY = 1776 - (lvl * 944);
            for (int sectorX = 0; sectorX < 944; sectorX += 48) {
                for (int sectorY = 0; sectorY < 944; sectorY += 48) {
                    int x = (sectorX + wildX) / 48;
                    int y = (sectorY + (lvl * 944) + wildY) / 48;
                    Point3D location = Point3D.builder().x(x).y(y).z(lvl).build();
                    sectors.put(
                            location,
                            Sector.builder().location(location).build()
                    );
                }
            }
        }

        Resource landscapeFile = resourceLoader.getResource("file:Authentic_P2PLandscape.orsc");
        ZipFile file = new ZipFile(landscapeFile.getFile());
        for (Map.Entry<Point3D, Sector> mapEntry : sectors.entrySet()) {
            Sector sector = mapEntry.getValue();
            ZipEntry entry = file.getEntry(sector.getName());
            BufferedInputStream in = new BufferedInputStream(file.getInputStream(entry));
            byte[] buffer = in.readAllBytes();
            sectorData.put(mapEntry.getKey(), buffer);
        }
    }

    private void unpackRegion(Sector sector) {
        long start = System.currentTimeMillis();
        ByteBuf buffer = Unpooled.wrappedBuffer(sectorData.get(sector.getLocation()));
        Tile[][] sectorTiles = new Tile[REGION_SIZE][REGION_SIZE];
        TileValue[][] sectorTileValues = new TileValue[REGION_SIZE][REGION_SIZE];
        try {
            for (int i = 0; i < REGION_SIZE; i++) {
                for (int j = 0; j < REGION_SIZE; j++) {
                    Tile tile = new Tile();
                    tile.setSector(sector);
                    tile.setX(i);
                    tile.setY(j);
                    tile.unpack(buffer);
                    sectorTiles[i][j] = tile;
                    sectorTileValues[i][j] = new TileValue();
                    Point rscCoords = tile.getRSCCoords();
                    rscCoordTileValue.put(rscCoords, new TileValue());
                    rscCoordTileTraversal.put(rscCoords, new TileTraversal(rscCoords));
                }
            }
            sector.setTiles(sectorTiles);
            sector.setTileValues(sectorTileValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        unpackRegionTotal += (end - start);
    }

    public TileTraversal getTileTraversal(int rscX, int rscY) {
        return rscCoordTileTraversal.computeIfAbsent(
                new Point(rscX, rscY),
                TileTraversal::new
        );
    }

    public TileTraversal getTileTraversal(Point point) {
        return getTileTraversal(point.getX(), point.getY());
    }

    public TileValue getTileValue(int rscX, int rscY) {
//        TileLocationInfo info = getTileLocationInfo(rscX, rscY);
//        Sector sector = sectors.get(info.getSectorLocation());
//        Point tileCoords = info.getSectorTileLocation();
//
//        return sector.getTileValues()[tileCoords.getX()][tileCoords.getY()];
        return rscCoordTileValue.computeIfAbsent(
                new Point(rscX, rscY),
                point -> new TileValue()
        );
    }

    private void initializeSectors() {
        long start = System.currentTimeMillis();
        for (Map.Entry<Point3D, Sector> mapEntry : sectors.entrySet()) {
            Sector sector = mapEntry.getValue();
            unpackRegion(sector);
            initializeSector(sector);
        }
        long end = System.currentTimeMillis();
        log.info("Sectors unpacked in " + (end - start) + " ms");
    }

    public boolean withinWorld(int x, int y) {
        return x >= 0 && x < MAX_WORLD_WIDTH && y >= 0 && y < MAX_WORLD_HEIGHT;
    }

    private void initializeSector(Sector sector) {
        long start = System.currentTimeMillis();
        Tile[][] tiles = sector.getTiles();
        for (int x = 0; x < REGION_SIZE; x++) {
            for (int y = 0; y < REGION_SIZE; y++) {
                initializeTileValue(tiles[x][y]);
            }
        }
        long end = System.currentTimeMillis();
        initializeSectorTotal += (end - start);
    }

    private void blockDirections(Point point, Direction... directions) {
        for (Direction direction : directions) {
            getTileTraversal(point).setIsBlocked(direction, true);
        }
    }

    private void initializeTileValue(Tile sectorTile) {
        Point coords = sectorTile.getRSCCoords();
        int bx = coords.getX();
        int by = coords.getY();

        if (!withinWorld(bx, by)) {
            return;
        }

        TileTraversal traversal = getTileTraversal(bx, by);
        TileValue tile = getTileValue(bx, by);

        tile.overlay = sectorTile.getGroundOverlay();
        tile.diagWallVal = sectorTile.getDiagonalWalls();
        tile.horizontalWallVal = sectorTile.getRightBorderWall();
        tile.verticalWallVal = sectorTile.getTopBorderWall();
        tile.elevation = sectorTile.getGroundElevation();

        if ((sectorTile.getGroundOverlay() & 0xff) == 250) {
            // Set to Water
            sectorTile.setGroundOverlay((byte) 2);
        }

        final byte groundOverlay = sectorTile.getGroundOverlay();
        if (groundOverlay > 0 && TileDef.getTileDef(groundOverlay - 1).getObjectType() != 0) {
            traversal.setTraversable(false);
        }

        final int verticalWall = Byte.toUnsignedInt(sectorTile.getRightBorderWall());
        if (verticalWall > 0
                && DoorDef.getDoorDef(verticalWall - 1).getUnknown() == 0
                && DoorDef.getDoorDef(verticalWall - 1).getDoorType() != 0) {
            blockDirections(coords, EAST);
            blockDirections(coords.move(EAST), WEST);

            if (projectileClipAllowed(verticalWall)) {
                tile.projectileAllowed = true;
            }
        }

        final int horizontalWall = Byte.toUnsignedInt(sectorTile.getTopBorderWall());
        if (horizontalWall > 0
                && DoorDef.getDoorDef(horizontalWall - 1).getUnknown() == 0
                && DoorDef.getDoorDef(horizontalWall - 1).getDoorType() != 0) {
            blockDirections(coords, Direction.NORTH);
            blockDirections(coords.move(Direction.NORTH), SOUTH);
            if (projectileClipAllowed(horizontalWall)) {
                tile.projectileAllowed = true;
                getTileValue(bx - 1, by).projectileAllowed = true;
            }
        }

        final int diagonalWalls = sectorTile.getDiagonalWalls();
        if (diagonalWalls > 0
                && diagonalWalls < 12000
                && DoorDef.getDoorDef(diagonalWalls - 1).getUnknown() == 0
                && DoorDef.getDoorDef(diagonalWalls - 1).getDoorType() != 0) {
            getTileTraversal(coords).setTraversable(false);
            if (projectileClipAllowed(diagonalWalls)) {
                tile.projectileAllowed = true;
            }
        }
        if (diagonalWalls > 12000
                && diagonalWalls < 24000
                && DoorDef.getDoorDef(diagonalWalls - 12001).getUnknown() == 0
                && DoorDef.getDoorDef(diagonalWalls - 12001).getDoorType() != 0) {
            getTileTraversal(coords).setTraversable(false);

            if (projectileClipAllowed(diagonalWalls)) {
                tile.projectileAllowed = true;
            }
        }

        if (tile.overlay == 2 || tile.overlay == 11) {
            tile.projectileAllowed = true;
        }
    }

    @Builder
    @Getter
    private static class TileLocationInfo {
        private final Point3D sectorLocation;
        private final Point sectorTileLocation;
        private final Point rscCoords;
    }
}
