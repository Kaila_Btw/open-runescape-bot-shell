package org.openrsc.orbs.util.data;

public class BitBoolean<T extends Enum<T>> {
    private byte mask;

    public BitBoolean() {
        this(false);
    }

    public BitBoolean(boolean defaultValues) {
        mask = defaultValues ? (byte) 0xFF : (byte) 0x00;
    }

    public boolean get(T obj) {
        byte index = (byte) obj.ordinal();
        return (mask >> index & 1) > 0;
    }

    public void set(T obj, boolean value) {
        byte index = (byte) obj.ordinal();
        byte mask = (byte) (1 << index);
        if (value) {
            this.mask |= mask;
        } else {
            this.mask &= (~mask & 0xFF);
        }
    }

    public BitBoolean<T> or(BitBoolean<T> bitBoolean) {
        BitBoolean<T> result = new BitBoolean<>();
        result.mask = mask;
        result.mask |= bitBoolean.mask;

        return result;
    }
}
