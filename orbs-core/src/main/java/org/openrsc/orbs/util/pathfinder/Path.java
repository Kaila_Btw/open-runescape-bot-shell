package org.openrsc.orbs.util.pathfinder;

import lombok.Builder;
import lombok.Getter;
import org.openrsc.orbs.model.Point;

import java.util.LinkedList;

@Builder
@Getter
public class Path {
    private final LinkedList<Point> points;
    // This is the point where the server should be able to take over for pathfinding
    private final Point releasePoint;
}
