package org.openrsc.orbs.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum Direction {
    NORTH(new Point(0, -1)),
    NORTHEAST(new Point(-1, -1)),
    EAST(new Point(-1, 0)),
    SOUTHEAST(new Point(-1, 1)),
    SOUTH(new Point(0, 1)),
    SOUTHWEST(new Point(1, 1)),
    WEST(new Point(1, 0)),
    NORTHWEST(new Point(1, -1));

    private static final Map<Point, Direction> byOffset;

    static {
        byOffset = new HashMap<>();
        for (Direction value : Direction.values()) {
            byOffset.put(value.offset, value);
        }
    }

    private final Point offset;

    public static Direction byOffset(Point offset) {
        return byOffset.get(offset);
    }

    public Point move(Direction direction, Point point) {
        Point offset = direction.getOffset();
        return new Point(point.getX() + offset.getX(), point.getY() + offset.getY());
    }

    public boolean isCardinal() {
        return ordinal() % 2 == 0;
    }

    public boolean isInterCardinal() {
        return !isCardinal();
    }

    public Pair<Direction, Direction> getBorderingOrdinals() {
        int counterClockwise = (ordinal() + 7) % 8;
        int clockwise = (ordinal() + 1) % 8;

        return Pair.of(
                values()[counterClockwise],
                values()[clockwise]
        );
    }

    public Pair<Direction, Direction> getBorderingCardinals() {
        int counterClockwise = (ordinal() + 6) % 8;
        int clockwise = (ordinal() + 2) % 8;

        return Pair.of(
                values()[counterClockwise],
                values()[clockwise]
        );
    }

    public Direction inverse() {
        return Direction.byOffset(offset.inverse());
    }
}
