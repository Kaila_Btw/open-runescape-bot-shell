package org.openrsc.orbs.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ItemDef {
    @JsonValue
    private final int id;
    private final String name;
    private final String description;
    private final String command;
    private final boolean isFemaleOnly;
    private final boolean isMembersOnly;
    private final boolean isStackable;
    private final boolean isUntradable;
    private final boolean isWearable;
    private final int appearanceID;
    private final int wearableID;
    private final int wearSlot;
    private final int requiredLevel;
    private final int requiredSkillID;
    private final long armourBonus;
    private final int weaponAimBonus;
    private final int weaponPowerBonus;
    private final int magicBonus;
    private final int prayerBonus;
    private final int basePrice;
    private final boolean isNoteable;
}
