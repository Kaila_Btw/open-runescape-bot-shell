package org.openrsc.orbs.model;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;
import org.openrsc.orbs.model.data.Sector;

@Getter
@Setter
public class Tile {
    private GameObjectLoc objectLoc;

    private Sector sector;

    private int ID = -1;
    private int x = -1;
    private int y = -1;
    private byte groundElevation = -1;
    private byte groundTexture = -1;
    private byte groundOverlay = -1;
    private byte roofTexture = -1;
    private byte rightBorderWall = -1;
    private byte topBorderWall = -1;
    private short diagonalWalls = -1;

    public Point getRSCCoords() {
        return new Point(
                (getX() + (sector.getLocation().getX() - 48) * 48),
                ((sector.getLocation().getY() - 36) * 48) + getY() - 48 + (sector.getLocation().getZ() * 944)
        );
    }

    /**
     * @param in - the Byte buffer holding every tile value.
     * @return - the new instance to the Tile with all properties set.
     */
    public Tile unpack(ByteBuf in) {
        groundElevation = in.readByte();
        groundTexture = in.readByte();
        groundOverlay = in.readByte();
        roofTexture = in.readByte();
        rightBorderWall = in.readByte();
        topBorderWall = in.readByte();
        diagonalWalls = (short) in.readInt();
        return this;
    }
}
