package org.openrsc.orbs.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum Skill {
    ATTACK(0),
    DEFENSE(1),
    STRENGTH(2),
    HITS(3),
    RANGED(4),
    PRAYER(5),
    MAGIC(6),
    COOKING(7),
    WOODCUT(8),
    FLETCHING(9),
    FISHING(10),
    FIREMAKING(11),
    CRAFTING(12),
    SMITHING(13),
    MINING(14),
    HERBLAW(15),
    AGILITY(16),
    THIEVING(17),
    RUNECRAFT(18),
    HARVESTING(19);

    private static final Map<Integer, Skill> skillMap = new HashMap<>();

    static {
        Arrays.stream(Skill.values()).forEach(skill -> skillMap.put(skill.getSkillId(), skill));
    }

    private final int skillId;

    public static Skill byId(Integer id) {
        return skillMap.get(id);
    }

    @Override
    @JsonValue
    public String toString() {
        return super.toString().toLowerCase();
    }
}
