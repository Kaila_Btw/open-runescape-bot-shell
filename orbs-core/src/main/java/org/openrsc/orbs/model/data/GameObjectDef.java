package org.openrsc.orbs.model.data;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class GameObjectDef extends EntityDef {
    public int type;
    public int width;
    public int height;
    public int modelID;
    private String command1;
    private String command2;
    private int groundItemVar;
    private String objectModel;

    public GameObjectDef(String name, String description, String command1, String command2, int type, int width, int height, int groundItemVar, String objectModel, int id) {
        super(name, description, id);
        this.command1 = command1;
        this.command2 = command2;
        this.type = type;
        this.width = width;
        this.height = height;
        this.groundItemVar = groundItemVar;
        this.objectModel = objectModel;
    }

    public static GameObjectDef getObjectDef(int id) {
        if (id < 0 || id >= GameObjectDefs.objects.size()) {
            return null;
        }
        return GameObjectDefs.objects.get(id);
    }

    public String getObjectModel() {
        return objectModel;
    }

    public String getCommand1() {
        return command1;
    }

    public String getCommand2() {
        return command2;
    }

    public int getType() {
        return type;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getGroundItemVar() {
        return groundItemVar;
    }
}
