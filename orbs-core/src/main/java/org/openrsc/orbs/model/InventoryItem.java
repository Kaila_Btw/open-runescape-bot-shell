package org.openrsc.orbs.model;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class InventoryItem {
    private final ItemDef def;
    private final boolean wielded;
    private final boolean noted;
    @Builder.Default
    private final int amount = 1;
}
