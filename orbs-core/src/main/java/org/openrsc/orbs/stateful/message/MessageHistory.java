package org.openrsc.orbs.stateful.message;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MessageHistory {
    private final int capacity;
    private final LinkedList<String> messages = new LinkedList<>();

    public MessageHistory(int capacity) {
        this.capacity = capacity;
    }

    public synchronized void add(String message) {
        if (messages.size() >= capacity) {
            messages.removeFirst();
        }
        messages.add(message);
    }

    @JsonValue
    public List<String> getMessages() {
        return Collections.unmodifiableList(messages);
    }
}
