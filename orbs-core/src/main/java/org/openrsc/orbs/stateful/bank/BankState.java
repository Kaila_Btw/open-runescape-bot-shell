package org.openrsc.orbs.stateful.bank;

import lombok.Getter;
import lombok.Setter;
import org.openrsc.orbs.net.dto.server.bank.BankSlot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
public class BankState {
    private final ArrayList<BankSlot> bankItems = new ArrayList<>();
    private final Map<Integer, Integer> itemIdToAmount = new HashMap<>();
    private boolean open = false;
    private Integer storedSize;
    private Integer maxSize;
}
