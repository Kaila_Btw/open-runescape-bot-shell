package org.openrsc.orbs.stateful.sound;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.dto.server.sound.PlaySoundEvent;
import org.openrsc.orbs.stateful.AbstractReducer;

@Log4j2
public class SoundReducer extends AbstractReducer {
    public SoundReducer(EventBus eventBus) {
        super(eventBus);
    }

    @Subscribe
    public void onSoundPlayed(PlaySoundEvent event) {
        log.info("Sound received {}", event.getSoundName());
    }
}
