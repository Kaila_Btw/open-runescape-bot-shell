package org.openrsc.orbs.stateful.menu;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.openrsc.orbs.net.dto.server.mob.npc.talk.MenuClosedEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.talk.MenuOpenedEvent;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.PlayerState;

import java.util.List;

public class MenuOptionReducer extends AbstractReducer {
    private final PlayerState playerState;

    public MenuOptionReducer(PlayerState playerState, EventBus eventBus) {
        super(eventBus);
        this.playerState = playerState;
    }

    @Subscribe
    public void onMenuOpened(MenuOpenedEvent event) {
        List<String> currentMenuOptions = playerState.getCurrentMenuOptions();
        currentMenuOptions.clear();
        currentMenuOptions.addAll(event.getOptions());
        playerState.getIsInMenu().set(true);
    }

    @Subscribe
    public void onMenuClosed(MenuClosedEvent event) {
        playerState.getCurrentMenuOptions().clear();
        playerState.getIsInMenu().set(false);
    }
}
