package org.openrsc.orbs.stateful.player;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.Synchronized;
import org.openrsc.orbs.net.dto.server.mob.player.PlayerVisualUpdateEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerAppearanceEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerBubbleEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerDamageEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerHpEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerOnNpcProjectileEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerOnPlayerProjectileEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerPublicChatEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerQuestChatEvent;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerUpdateEvent;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.model.Player;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class PlayerVisualReducer extends AbstractReducer {
    private final PlayerState playerState;
    private final EventBus eventBus;

    public PlayerVisualReducer(PlayerState playerState, EventBus eventBus) {
        super(eventBus);
        this.playerState = playerState;
        this.eventBus = eventBus;
    }

    @Subscribe
    public void onPlayerVisualUpdateEvent(PlayerVisualUpdateEvent event) {
        for (PlayerUpdateEvent updateEvent : event.getUpdateEvents()) {
            eventBus.post(updateEvent);
        }
    }

    @Subscribe
    public void onPlayerAppearanceEvent(PlayerAppearanceEvent event) {
        if (event.getUsername().equalsIgnoreCase(playerState.getUsername())) {
            playerState.getPlayerId().set(event.getPlayerId());
        }
        updatePlayer(
                event,
                player -> player.toBuilder()
                        .playerId(event.getPlayerId())
                        .username(event.getUsername())
                        .wornItemAppearanceIds(event.getWornItemAppearanceIds())
                        .hairColor(event.getHairColor())
                        .topColor(event.getTopColor())
                        .pantsColor(event.getPantsColor())
                        .skinColor(event.getSkinColor())
                        .combatLevel(event.getCombatLevel())
                        .skulled(event.isSkulled())
                        .clanTag(event.getClanTag())
                        .invisible(event.isInvisible())
                        .invulnerable(event.isInvulnerable())
                        .groupId(event.getGroupId())
                        .icon(event.getIcon())
                        .build()
        );
    }

    @Subscribe
    public void onPlayerBubbleEvent(PlayerBubbleEvent event) {
        // Throw out this event for now
    }

    @Subscribe
    public void onPlayerDamageEvent(PlayerDamageEvent event) {
        // on player damaged function for script call?
        updatePlayer(
                event,
                player -> player.toBuilder()
                        .currentHp(event.getCurrentHp())
                        .maxHp(event.getCurrentHp())
                        .build()
        );
    }

    @Subscribe
    public void onPlayerHpEvent(PlayerHpEvent event) {
        updatePlayer(
                event,
                player -> player.toBuilder()
                        .currentHp(event.getCurrentHp())
                        .maxHp(event.getMaxHp())
                        .build()
        );
    }

    @Subscribe
    public void onPlayerPlayerProjectileEvent(PlayerOnPlayerProjectileEvent event) {
        // Script event method?
    }

    @Subscribe
    public void onPlayerNpcProjectileEvent(PlayerOnNpcProjectileEvent event) {
        // Script event method?
    }

    @Subscribe
    public void onPlayerPublicChatEvent(PlayerPublicChatEvent event) {
        // Chat event method?
    }

    @Subscribe
    public void onPlayerQuestChatEvent(PlayerQuestChatEvent event) {
        // Quest chat event method?
    }

    @Synchronized
    public void updatePlayer(PlayerUpdateEvent event, Function<Player, Player> playerUpdater) {
        Integer pid = event.getPlayerId();
        Map<Integer, Player> playerInfoByPid = playerState.getPlayerInfoByPid();
        Player current = Optional.ofNullable(playerInfoByPid.get(pid)).orElse(Player.builder().build());
        playerInfoByPid.put(pid, playerUpdater.apply(current));
    }
}
