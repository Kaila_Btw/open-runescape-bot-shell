package org.openrsc.orbs.stateful.player.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.model.Locatable;
import org.openrsc.orbs.model.Point;

@AllArgsConstructor
@Getter
public class NpcLocation implements Locatable {
    private final Point location;
    private final int npcId;
}
