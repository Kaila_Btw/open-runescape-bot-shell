package org.openrsc.orbs.stateful.skills;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.model.Skill;
import org.openrsc.orbs.net.connection.player.script.event.LevelUpEvent;
import org.openrsc.orbs.net.dto.server.stat.SendStatEvent;
import org.openrsc.orbs.net.dto.server.stat.SendStatsEvent;
import org.openrsc.orbs.stateful.AbstractReducer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
public class StatsReducer extends AbstractReducer {
    private final SkillState skillState;
    private final EventBus scriptEventBus;

    public StatsReducer(SkillState skillState, EventBus eventBus, EventBus scriptEventBus) {
        super(eventBus);
        this.skillState = skillState;
        this.scriptEventBus = scriptEventBus;
    }

    @Subscribe
    public void onStatsReceived(SendStatsEvent statsEvent) {
        Map<Skill, Integer> currentLevels = new ConcurrentHashMap<>();
        currentLevels.put(Skill.ATTACK, statsEvent.getCurrentAttack());
        currentLevels.put(Skill.DEFENSE, statsEvent.getCurrentDefense());
        currentLevels.put(Skill.STRENGTH, statsEvent.getCurrentStrength());
        currentLevels.put(Skill.HITS, statsEvent.getCurrentHits());
        currentLevels.put(Skill.RANGED, statsEvent.getCurrentRanged());
        currentLevels.put(Skill.PRAYER, statsEvent.getCurrentPrayer());
        currentLevels.put(Skill.MAGIC, statsEvent.getCurrentMagic());
        currentLevels.put(Skill.COOKING, statsEvent.getCurrentCooking());
        currentLevels.put(Skill.WOODCUT, statsEvent.getCurrentWoodcutting());
        currentLevels.put(Skill.FLETCHING, statsEvent.getCurrentFletching());
        currentLevels.put(Skill.FISHING, statsEvent.getCurrentFishing());
        currentLevels.put(Skill.FIREMAKING, statsEvent.getCurrentFiremaking());
        currentLevels.put(Skill.CRAFTING, statsEvent.getCurrentCrafting());
        currentLevels.put(Skill.SMITHING, statsEvent.getCurrentSmithing());
        currentLevels.put(Skill.MINING, statsEvent.getCurrentMining());
        currentLevels.put(Skill.HERBLAW, statsEvent.getCurrentHerblaw());
        currentLevels.put(Skill.AGILITY, statsEvent.getCurrentAgility());
        currentLevels.put(Skill.THIEVING, statsEvent.getCurrentThieving());
        skillState.getCurrentLevels().clear();
        skillState.getCurrentLevels().putAll(currentLevels);

        Map<Skill, Integer> maxLevels = new ConcurrentHashMap<>();
        maxLevels.put(Skill.ATTACK, statsEvent.getMaxAttack());
        maxLevels.put(Skill.DEFENSE, statsEvent.getMaxDefense());
        maxLevels.put(Skill.STRENGTH, statsEvent.getMaxStrength());
        maxLevels.put(Skill.HITS, statsEvent.getMaxHits());
        maxLevels.put(Skill.RANGED, statsEvent.getMaxRanged());
        maxLevels.put(Skill.PRAYER, statsEvent.getMaxPrayer());
        maxLevels.put(Skill.MAGIC, statsEvent.getMaxMagic());
        maxLevels.put(Skill.COOKING, statsEvent.getMaxCooking());
        maxLevels.put(Skill.WOODCUT, statsEvent.getMaxWoodcutting());
        maxLevels.put(Skill.FLETCHING, statsEvent.getMaxFletching());
        maxLevels.put(Skill.FISHING, statsEvent.getMaxFishing());
        maxLevels.put(Skill.FIREMAKING, statsEvent.getMaxFiremaking());
        maxLevels.put(Skill.CRAFTING, statsEvent.getMaxCrafting());
        maxLevels.put(Skill.SMITHING, statsEvent.getMaxSmithing());
        maxLevels.put(Skill.MINING, statsEvent.getMaxMining());
        maxLevels.put(Skill.HERBLAW, statsEvent.getMaxHerblaw());
        maxLevels.put(Skill.AGILITY, statsEvent.getMaxAgility());
        maxLevels.put(Skill.THIEVING, statsEvent.getMaxThieving());
        skillState.getMaxLevels().clear();
        skillState.getMaxLevels().putAll(maxLevels);

        Map<Skill, Integer> currentExperience = new ConcurrentHashMap<>();
        currentExperience.put(Skill.ATTACK, statsEvent.getExperienceAttack());
        currentExperience.put(Skill.DEFENSE, statsEvent.getExperienceDefense());
        currentExperience.put(Skill.STRENGTH, statsEvent.getExperienceStrength());
        currentExperience.put(Skill.HITS, statsEvent.getExperienceHits());
        currentExperience.put(Skill.RANGED, statsEvent.getExperienceRanged());
        currentExperience.put(Skill.PRAYER, statsEvent.getExperiencePrayer());
        currentExperience.put(Skill.MAGIC, statsEvent.getExperienceMagic());
        currentExperience.put(Skill.COOKING, statsEvent.getExperienceCooking());
        currentExperience.put(Skill.WOODCUT, statsEvent.getExperienceWoodcutting());
        currentExperience.put(Skill.FLETCHING, statsEvent.getExperienceFletching());
        currentExperience.put(Skill.FISHING, statsEvent.getExperienceFishing());
        currentExperience.put(Skill.FIREMAKING, statsEvent.getExperienceFiremaking());
        currentExperience.put(Skill.CRAFTING, statsEvent.getExperienceCrafting());
        currentExperience.put(Skill.SMITHING, statsEvent.getExperienceSmithing());
        currentExperience.put(Skill.MINING, statsEvent.getExperienceMining());
        currentExperience.put(Skill.HERBLAW, statsEvent.getExperienceHerblaw());
        currentExperience.put(Skill.AGILITY, statsEvent.getExperienceAgility());
        currentExperience.put(Skill.THIEVING, statsEvent.getExperienceThieving());

        final Map<Skill, Integer> currentXPMap = skillState.getCurrentExperience();
        currentXPMap.clear();
        currentXPMap.putAll(currentExperience);

        skillState.setQuestPoints(statsEvent.getQuestPoints());
    }

    @Subscribe
    public void onStatUpdateEvent(SendStatEvent event) {
        final Skill skill = Skill.byId(event.getStatId());
        skillState.getCurrentExperience().put(skill, event.getExperience());
        if (event.getMaxLevel() != skillState.getMaxLevels().get(skill)) {
            scriptEventBus.post(
                    LevelUpEvent.builder()
                            .skill(skill)
                            .level(event.getMaxLevel())
            );
        }
        skillState.getMaxLevels().put(skill, event.getMaxLevel());
        skillState.getCurrentLevels().put(skill, event.getCurrentLevel());
    }
}
