package org.openrsc.orbs.stateful.sleep;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.connection.player.script.event.SleepStarted;
import org.openrsc.orbs.net.connection.player.script.event.SleepStopped;
import org.openrsc.orbs.net.connection.player.script.event.SleepWordProcessed;
import org.openrsc.orbs.net.dto.server.sleep.SleepFatigueEvent;
import org.openrsc.orbs.net.dto.server.sleep.SleepWordEvent;
import org.openrsc.orbs.net.dto.server.sleep.StopSleepScreenEvent;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.PlayerState;
import org.openrsc.orbs.util.ocr.SleepWordProcessor;
import org.openrsc.orbs.util.ocr.stormy.ocrlib.OCRException;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;

@Log4j2
public class SleepReducer extends AbstractReducer {
    private final PlayerState playerState;
    private final EventBus scriptEventBus;
    private final SleepWordProcessor processor;

    public SleepReducer(
            PlayerState playerState,
            EventBus eventBus,
            EventBus scriptEventBus,
            ResourceLoader resourceLoader
    ) throws IOException, OCRException {
        super(eventBus);
        this.playerState = playerState;
        this.scriptEventBus = scriptEventBus;
        processor = new SleepWordProcessor(resourceLoader);
    }

    @Subscribe
    public void onSleepWordEvent(SleepWordEvent event) {
        log.info("Sleep word received...");
        playerState.getIsSleeping().set(true);
        scriptEventBus.post(new SleepStarted());
        String word = processor.getSleepWord(event.getImage()).orElse("");
        log.info("Guessing sleepword: " + word);
        playerState.getSleepWord().set(word);
        scriptEventBus.post(SleepWordProcessed.builder().sleepWord(word).build());
    }

    @Subscribe
    public void onStopSleepEvent(StopSleepScreenEvent event) {
        playerState.getIsSleeping().set(false);
        playerState.getSleepWord().set(null);
        playerState.getSleepScreenFatigue().set(100);
        scriptEventBus.post(new SleepStopped());
    }

    @Subscribe
    public void onSleepFatigueEvent(SleepFatigueEvent event) {
        playerState.getSleepScreenFatigue().set(event.getFatigue());
        scriptEventBus.post(event);
    }
}
