package org.openrsc.orbs.stateful.player;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import lombok.Getter;
import lombok.Setter;
import org.openrsc.orbs.model.Locatable;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.model.TileTraversal;
import org.openrsc.orbs.net.dto.server.config.ServerConfigurationEvent;
import org.openrsc.orbs.stateful.bank.BankState;
import org.openrsc.orbs.stateful.inventory.InventoryState;
import org.openrsc.orbs.stateful.message.MessageHistory;
import org.openrsc.orbs.stateful.objects.GameObjectsState;
import org.openrsc.orbs.stateful.player.model.NpcInfo;
import org.openrsc.orbs.stateful.player.model.NpcLocation;
import org.openrsc.orbs.stateful.player.model.Player;
import org.openrsc.orbs.stateful.skills.SkillState;
import org.openrsc.orbs.stateful.trade.TradeState;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@Setter
public class PlayerState implements Locatable {
    private final AtomicReference<Integer> playerId = new AtomicReference<>(-1);
    private final InventoryState inventory = new InventoryState();
    private final BankState bank = new BankState();
    private final TradeState trade = new TradeState();
    private final AtomicReference<Boolean> isSleeping = new AtomicReference<>(false);
    private final AtomicReference<Boolean> isInCombat = new AtomicReference<>(false);
    private final AtomicReference<Boolean> isInMenu = new AtomicReference<>(false);
    private final AtomicReference<Integer> fatigue = new AtomicReference<>();
    private final SkillState skills = new SkillState();
    private final GameObjectsState gameObjects = new GameObjectsState();
    private final Multimap<Integer, Point> groundItems = LinkedListMultimap.create();
    private final Map<Integer, Point> playersByPid = Collections.synchronizedMap(new LinkedHashMap<>());
    private final Map<Integer, Player> playerInfoByPid = Collections.synchronizedMap(new LinkedHashMap<>());
    private final Map<Integer, Boolean> playersInCombat = Collections.synchronizedMap(new LinkedHashMap<>());
    private final Map<Integer, NpcLocation> npcsByServerId = Collections.synchronizedMap(new LinkedHashMap<>());
    private final Map<Integer, NpcInfo> npcInfoByServerId = Collections.synchronizedMap(new LinkedHashMap<>());
    private final AtomicReference<Integer> sleepScreenFatigue = new AtomicReference<>();
    private final AtomicReference<String> sleepWord = new AtomicReference<>();
    @JsonIgnore
    private final Map<Point, TileTraversal> objectTraversalMap = new ConcurrentHashMap<>();
    private final List<String> currentMenuOptions = new CopyOnWriteArrayList<>();
    private final MessageHistory messageHistory = new MessageHistory(7);
    private volatile Point location;
    private volatile String username;
    private ServerConfigurationEvent serverConfiguration;
    private volatile String status;
}
