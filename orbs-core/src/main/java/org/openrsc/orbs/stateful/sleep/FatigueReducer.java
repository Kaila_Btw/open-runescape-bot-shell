package org.openrsc.orbs.stateful.sleep;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.dto.server.sleep.FatigueEvent;
import org.openrsc.orbs.stateful.AbstractReducer;

import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class FatigueReducer extends AbstractReducer {
    private final AtomicReference<Integer> fatigue;
    private final EventBus scriptEventBus;

    public FatigueReducer(AtomicReference<Integer> fatigue, EventBus eventBus, EventBus scriptEventBus) {
        super(eventBus);
        this.fatigue = fatigue;
        this.scriptEventBus = scriptEventBus;
    }

    @Subscribe
    public void onFatigueEvent(FatigueEvent fatigueEvent) {
        fatigue.set(fatigueEvent.getFatigue());
        scriptEventBus.post(fatigueEvent);
    }
}
