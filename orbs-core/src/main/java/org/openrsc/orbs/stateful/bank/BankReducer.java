package org.openrsc.orbs.stateful.bank;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.openrsc.orbs.net.dto.server.bank.BankCloseEvent;
import org.openrsc.orbs.net.dto.server.bank.BankOpenEvent;
import org.openrsc.orbs.net.dto.server.bank.BankSlot;
import org.openrsc.orbs.net.dto.server.bank.BankUpdateEvent;
import org.openrsc.orbs.stateful.AbstractReducer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BankReducer extends AbstractReducer {
    private final BankState bankState;

    public BankReducer(BankState bankState, EventBus eventBus) {
        super(eventBus);
        this.bankState = bankState;
    }

    @Subscribe
    public void onBankOpenEvent(BankOpenEvent event) {
        bankState.setOpen(true);
        bankState.setStoredSize(event.getItemsStoredSize());
        bankState.setMaxSize(event.getMaxBankSize());
        List<BankSlot> bankItems = bankState.getBankItems();
        Map<Integer, Integer> bankSlots = bankState.getItemIdToAmount();
        bankItems.clear();
        bankItems.addAll(event.getBankSlot());
        bankSlots.clear();
        event.getBankSlot().forEach(
                bankSlot -> bankSlots.put(
                        bankSlot.getCatalogId(),
                        bankSlot.getAmount()
                )
        );
    }

    @Subscribe
    public void onBankUpdateEvent(BankUpdateEvent event) {
        ArrayList<BankSlot> bankItems = bankState.getBankItems();
        if (event.getAmount() == 0) {
            bankItems.remove(event.getSlotId());
        } else {
            BankSlot newBankSlot = BankSlot.builder()
                    .catalogId(event.getItemId())
                    .amount(event.getAmount())
                    .build();
            if (event.getSlotId() > bankItems.size() - 1) {
                bankItems.add(newBankSlot);
            } else {
                bankItems.set(
                        event.getSlotId(),
                        newBankSlot
                );
            }
        }
        Map<Integer, Integer> bankSlots = bankState.getItemIdToAmount();
        bankSlots.clear();
        bankItems.forEach(
                bankSlot -> bankSlots.put(
                        bankSlot.getCatalogId(),
                        bankSlot.getAmount()
                )
        );
    }

    @Subscribe
    public void onBankClosedEvent(BankCloseEvent event) {
        bankState.setOpen(false);
    }
}
