package org.openrsc.orbs.stateful.player;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcCoordsEvent;
import org.openrsc.orbs.net.dto.server.mob.player.PlayerCoordsEvent;
import org.openrsc.orbs.net.util.DataConversions;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.model.NpcLocation;

import java.util.Iterator;
import java.util.Map;

@Getter
@Log4j2
public class PlayerReducer extends AbstractReducer {
    private final PlayerState playerState;

    public PlayerReducer(PlayerState playerState, EventBus eventBus) {
        super(eventBus);
        this.playerState = playerState;
    }

    @Subscribe
    public void onPlayerCoordsReceived(PlayerCoordsEvent playerCoordsEvent) {
        // Update current player location & isInCombat
        playerState.setLocation(
                Point.fromDTO(playerCoordsEvent.getCurrentPlayerLocation())
        );
        playerState.getIsInCombat().set(playerCoordsEvent.isInCombat());

        Iterator<Map.Entry<Integer, Point>> playerIterator = playerState.getPlayersByPid().entrySet().iterator();
        for (int i = 0; playerIterator.hasNext(); i++) {
            playerIterator.next();
            if (playerCoordsEvent.getRemovedPlayers().contains(i)) {
                playerIterator.remove();
            }
        }

        // Add players
        playerCoordsEvent.getPlayerStatusUpdates().forEach(
                (pid, point) -> playerState.getPlayersByPid().put(pid, Point.fromDTO(point))
        );
        playerCoordsEvent.getPlayersInCombat().forEach(playerState.getPlayersInCombat()::put);
    }

    @Subscribe
    public void onNpcCoordsReceived(NpcCoordsEvent npcCoordsEvent) {
        // Get Npc coords
        Iterator<Map.Entry<Integer, NpcLocation>> npcIterator = playerState.getNpcsByServerId().entrySet().iterator();
        for (int i = 0; npcIterator.hasNext(); i++) {
            npcIterator.next();
            if (npcCoordsEvent.getRemovedNpcs().contains(i)) {
                npcIterator.remove();
            }
        }

        // Add players
        npcCoordsEvent.getNpcStatusUpdates().forEach(
                (serverId, npcPositionUpdate) -> {
                    Point updatedLocation = DataConversions.reverseCoordOffsets(
                            npcPositionUpdate.getOffsetX(),
                            npcPositionUpdate.getOffsetY(),
                            playerState.getLocation()
                    );
                    playerState.getNpcsByServerId().put(serverId, new NpcLocation(updatedLocation, npcPositionUpdate.getNpcId()));
                }
        );
    }
}
