package org.openrsc.orbs.stateful.trade;

import lombok.Getter;
import lombok.Setter;
import org.openrsc.orbs.net.connection.player.script.model.Item;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Setter
@Getter
public class TradeState {
    private TradeStatus status = TradeStatus.NOT_TRADING;
    private volatile boolean accepted = false;
    private volatile boolean otherPlayerAccepted = false;
    private List<Item> offeredItems = new CopyOnWriteArrayList<>();
    private List<Item> otherPlayerOfferedItems = new CopyOnWriteArrayList<>();
    private volatile int otherPlayerPlayerId;
}
