package org.openrsc.orbs.net.mapper.net;

import org.mapstruct.Mapper;
import org.openrsc.orbs.model.ItemDef;
import org.openrsc.orbs.net.connection.player.script.model.Item;
import org.openrsc.orbs.net.connection.player.script.model.ItemSet;
import org.openrsc.orbs.net.dto.client.trade.TradeItem;
import org.openrsc.orbs.util.EntityService;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface TradeItemMapper {
    default List<TradeItem> toTradeItems(ItemSet itemSet) {
        List<TradeItem> tradeItems = new ArrayList<>();
        for (Item item : itemSet.getItems()) {
            int itemId = item.getId();
            ItemDef itemDef = EntityService.getItemDef(itemId);
            if (itemDef.isStackable() || item.isNoted()) {
                tradeItems.add(
                        TradeItem.builder()
                                .itemId(itemId)
                                .amount(item.getAmount())
                                .build()
                );
            } else {
                for (int i = 0; i < item.getAmount(); i++) {
                    tradeItems.add(
                            TradeItem.builder()
                                    .itemId(itemId)
                                    .amount(1)
                                    .build()
                    );
                }
            }
        }
        return tradeItems;
    }
}
