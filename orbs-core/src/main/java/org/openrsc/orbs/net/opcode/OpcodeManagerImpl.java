package org.openrsc.orbs.net.opcode;

import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.annotation.RegisterEventOpcodes;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
@Log4j2
public class OpcodeManagerImpl implements OpcodeManager {
    private final Map<ClientOpcode, Class<?>> clientActionTypes = new HashMap<>();
    private final Map<ServerOpcode, Class<?>> serverEventTypes = new HashMap<>();

    public OpcodeManagerImpl() {
        Reflections reflections = new Reflections("org.openrsc.orbs.net.dto", new SubTypesScanner(), new TypeAnnotationsScanner());
        Set<Class<?>> actionOpcodes = reflections.getTypesAnnotatedWith(RegisterActionOpcode.class);
        actionOpcodes.forEach(type -> {
            RegisterActionOpcode actionOpcode = type.getAnnotation(RegisterActionOpcode.class);
            clientActionTypes.put(actionOpcode.value(), type);
        });

        Set<Class<?>> eventOpcodes = reflections.getTypesAnnotatedWith(RegisterEventOpcode.class);
        eventOpcodes.forEach(type -> {
            RegisterEventOpcode eventOpcode = type.getAnnotation(RegisterEventOpcode.class);
            serverEventTypes.put(eventOpcode.value(), type);
        });

        Set<Class<?>> multiOpCodes = reflections.getTypesAnnotatedWith(RegisterEventOpcodes.class);
        multiOpCodes.forEach(type -> {
            RegisterEventOpcodes opcodes = type.getAnnotation(RegisterEventOpcodes.class);
            for (RegisterEventOpcode eventOpcode : opcodes.value()) {
                serverEventTypes.put(eventOpcode.value(), type);
            }
        });
    }

    @Override
    public Class<?> getType(ClientOpcode opcode) {
        return clientActionTypes.get(opcode);
    }

    @Override
    public Class<?> getType(ServerOpcode opcode) {
        return serverEventTypes.get(opcode);
    }
}
