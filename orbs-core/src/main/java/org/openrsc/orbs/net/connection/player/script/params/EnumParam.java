package org.openrsc.orbs.net.connection.player.script.params;

import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@Getter
public class EnumParam<T extends Enum<T>> extends ScriptParam {
    @NonNull
    private final T[] values;

    public List<SelectableValue<T>> getValues() {
        if (values != null && values.length > 0 && values[0] instanceof Itemable) {
            return SelectableItem.of(values);
        }
        return SelectableValue.of(values);
    }

    @Override
    public ScriptParamType getType() {
        return ScriptParamType.ENUM;
    }
}