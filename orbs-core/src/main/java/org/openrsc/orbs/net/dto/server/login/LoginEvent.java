package org.openrsc.orbs.net.dto.server.login;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.LoginEventDeserializer;
import org.openrsc.orbs.stateful.player.model.LoginResponse;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = LoginEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.RAW_RESPONSE)
public class LoginEvent extends Event {
    private final LoginResponse loginResponse;
}
