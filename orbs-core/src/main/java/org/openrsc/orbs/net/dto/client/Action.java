package org.openrsc.orbs.net.dto.client;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketIgnore;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@Getter
@SuperBuilder
public abstract class Action {
    @PacketIgnore
    private final ClientOpcode opcode;
}
