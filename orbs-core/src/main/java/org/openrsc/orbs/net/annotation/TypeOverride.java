package org.openrsc.orbs.net.annotation;

import org.openrsc.orbs.net.annotation.model.DataType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TypeOverride {
    DataType value();
}
