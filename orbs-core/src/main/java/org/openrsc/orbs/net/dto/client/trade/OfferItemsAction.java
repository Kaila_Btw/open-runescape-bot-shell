package org.openrsc.orbs.net.dto.client.trade;

import lombok.Getter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

import java.util.List;

@SuperBuilder
@Getter
@RegisterActionOpcode(ClientOpcode.PLAYER_ADDED_ITEMS_TO_TRADE_OFFER)
@PacketSerializable
public class OfferItemsAction extends Action {
    @Singular
    private final List<TradeItem> items;
}
