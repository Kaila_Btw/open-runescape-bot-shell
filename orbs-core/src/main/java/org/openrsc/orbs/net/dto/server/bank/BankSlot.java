package org.openrsc.orbs.net.dto.server.bank;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;

@SuperBuilder(toBuilder = true)
@Getter
@PacketSerializable
public class BankSlot {
    @TypeOverride(DataType.SHORT)
    private final int catalogId;
    private final int amount;
}
