package org.openrsc.orbs.net.dto.server.stat;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

@SuperBuilder
@Getter
@RegisterEventOpcode(ServerOpcode.SEND_STATS)
@PacketSerializable
public class SendStatsEvent extends Event {
    // Current Level
    @TypeOverride(DataType.BYTE)
    private final int currentAttack;
    @TypeOverride(DataType.BYTE)
    private final int currentDefense;
    @TypeOverride(DataType.BYTE)
    private final int currentStrength;
    @TypeOverride(DataType.BYTE)
    private final int currentHits;
    @TypeOverride(DataType.BYTE)
    private final int currentRanged;
    @TypeOverride(DataType.BYTE)
    private final int currentPrayer;
    @TypeOverride(DataType.BYTE)
    private final int currentMagic;
    @TypeOverride(DataType.BYTE)
    private final int currentCooking;
    @TypeOverride(DataType.BYTE)
    private final int currentWoodcutting;
    @TypeOverride(DataType.BYTE)
    private final int currentFletching;
    @TypeOverride(DataType.BYTE)
    private final int currentFishing;
    @TypeOverride(DataType.BYTE)
    private final int currentFiremaking;
    @TypeOverride(DataType.BYTE)
    private final int currentCrafting;
    @TypeOverride(DataType.BYTE)
    private final int currentSmithing;
    @TypeOverride(DataType.BYTE)
    private final int currentMining;
    @TypeOverride(DataType.BYTE)
    private final int currentHerblaw;
    @TypeOverride(DataType.BYTE)
    private final int currentAgility;
    @TypeOverride(DataType.BYTE)
    private final int currentThieving;

    // Max Level
    @TypeOverride(DataType.BYTE)
    private final int maxAttack;
    @TypeOverride(DataType.BYTE)
    private final int maxDefense;
    @TypeOverride(DataType.BYTE)
    private final int maxStrength;
    @TypeOverride(DataType.BYTE)
    private final int maxHits;
    @TypeOverride(DataType.BYTE)
    private final int maxRanged;
    @TypeOverride(DataType.BYTE)
    private final int maxPrayer;
    @TypeOverride(DataType.BYTE)
    private final int maxMagic;
    @TypeOverride(DataType.BYTE)
    private final int maxCooking;
    @TypeOverride(DataType.BYTE)
    private final int maxWoodcutting;
    @TypeOverride(DataType.BYTE)
    private final int maxFletching;
    @TypeOverride(DataType.BYTE)
    private final int maxFishing;
    @TypeOverride(DataType.BYTE)
    private final int maxFiremaking;
    @TypeOverride(DataType.BYTE)
    private final int maxCrafting;
    @TypeOverride(DataType.BYTE)
    private final int maxSmithing;
    @TypeOverride(DataType.BYTE)
    private final int maxMining;
    @TypeOverride(DataType.BYTE)
    private final int maxHerblaw;
    @TypeOverride(DataType.BYTE)
    private final int maxAgility;
    @TypeOverride(DataType.BYTE)
    private final int maxThieving;

    // Experience
    private final int experienceAttack;
    private final int experienceDefense;
    private final int experienceStrength;
    private final int experienceHits;
    private final int experienceRanged;
    private final int experiencePrayer;
    private final int experienceMagic;
    private final int experienceCooking;
    private final int experienceWoodcutting;
    private final int experienceFletching;
    private final int experienceFishing;
    private final int experienceFiremaking;
    private final int experienceCrafting;
    private final int experienceSmithing;
    private final int experienceMining;
    private final int experienceHerblaw;
    private final int experienceAgility;
    private final int experienceThieving;

    @TypeOverride(DataType.BYTE)
    private final int questPoints;
}
