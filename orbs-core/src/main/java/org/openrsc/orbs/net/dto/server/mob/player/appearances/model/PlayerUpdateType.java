package org.openrsc.orbs.net.dto.server.mob.player.appearances.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum PlayerUpdateType {
    BUBBLE(0),
    PUBLIC_CHAT(1),
    DAMAGE_UPDATE(2),
    PROJECTILE_NPC(3),
    PROJECTILE_PLAYER(4),
    PLAYER_APPEARANCE_IDENTITY(5),
    QUEST_CHAT(6),
    MUTED_CHAT(7),
    HP_UPDATE(9);

    private static final Map<Integer, PlayerUpdateType> byId = new HashMap<>();

    static {
        Arrays.stream(PlayerUpdateType.values()).forEach(type -> byId.put(type.getUpdateType(), type));
    }

    private final int updateType;

    public static PlayerUpdateType byId(int id) {
        return byId.get(id);
    }
}
