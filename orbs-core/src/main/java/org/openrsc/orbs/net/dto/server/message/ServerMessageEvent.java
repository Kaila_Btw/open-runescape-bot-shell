package org.openrsc.orbs.net.dto.server.message;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerIcon;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.ServerMessageEventDeserializer;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = ServerMessageEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_SERVER_MESSAGE)
public class ServerMessageEvent extends Event {
    private final PlayerIcon icon;
    private final MessageType messageType;
    private final String message;
    private final String sender;
    private final String colorString;
}