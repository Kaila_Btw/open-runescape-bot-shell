package org.openrsc.orbs.net.opcode;

public interface OpcodeManager {
    Class<?> getType(ClientOpcode opcode);

    Class<?> getType(ServerOpcode opcode);
}
