package org.openrsc.orbs.net.dto.client.trade;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@SuperBuilder
@Getter
@RegisterActionOpcode(ClientOpcode.PLAYER_INIT_TRADE_REQUEST)
@PacketSerializable
public class SendTradeRequestAction extends Action {
    @TypeOverride(DataType.SHORT)
    private final int targetPlayerId;
}
