package org.openrsc.orbs.net.dto.server.mob.player;

import lombok.Getter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.PlayerUpdateEvent;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.PlayerVisualUpdateEventDeserializer;

import java.util.List;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = PlayerVisualUpdateEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_UPDATE_PLAYERS)
public class PlayerVisualUpdateEvent extends Event {
    @Singular
    List<PlayerUpdateEvent> updateEvents;
}
