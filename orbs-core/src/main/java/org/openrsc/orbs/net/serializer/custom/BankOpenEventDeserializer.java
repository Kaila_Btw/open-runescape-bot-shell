package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.net.dto.server.bank.BankOpenEvent;
import org.openrsc.orbs.net.dto.server.bank.BankSlot;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

import java.util.ArrayList;
import java.util.List;

public class BankOpenEventDeserializer implements PacketDeserializer<BankOpenEvent> {
    @Override
    public BankOpenEvent deserialize(Packet packet, Class<?> type) {
        int storedSize = Short.toUnsignedInt(packet.readShort());
        int maxBankSize = Short.toUnsignedInt(packet.readShort());
        List<BankSlot> bankSlots = new ArrayList<>();
        for (int i = 0; i < storedSize; i++) {
            bankSlots.add(
                    BankSlot.builder()
                            .catalogId(Short.toUnsignedInt(packet.readShort()))
                            .amount(packet.readInt())
                            .build()
            );
        }

        return BankOpenEvent.builder()
                .maxBankSize(maxBankSize)
                .itemsStoredSize(storedSize)
                .bankSlot(bankSlots)
                .build();
    }
}
