package org.openrsc.orbs.net.dto.server.bank;

import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

@SuperBuilder
@PacketSerializable
@RegisterEventOpcode(ServerOpcode.SEND_BANK_CLOSE)
public class BankCloseEvent extends Event {
}
