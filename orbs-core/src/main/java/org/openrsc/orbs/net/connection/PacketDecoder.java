package org.openrsc.orbs.net.connection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.model.Packet;

import java.util.List;

@Log4j2
public class PacketDecoder extends ByteToMessageDecoder {
    private final ByteBufAllocator alloc = PooledByteBufAllocator.DEFAULT;

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        Boolean waitingForLoginResponse = ctx.channel().attr(ConnectionHandler.WAITING_FOR_LOGIN_KEY).get();
        if (in.readableBytes() >= 2) {
            in.markReaderIndex();
            int length = in.readShort() - 2;
            if (in.readableBytes() >= length && length > 0) {
                int opcode = (in.readByte()) & 0xFF;
                length -= 1;
                ByteBuf data = alloc.buffer(length);
                in.readBytes(data, length);
                Packet packet = new Packet(opcode, data);
                out.add(packet);
            } else {
                in.resetReaderIndex();
            }
        } else {
            if (waitingForLoginResponse) {
                // Raw packet
                ByteBuf data = alloc.buffer(1);
                in.readBytes(data, 1);
                Packet packet = new Packet(-1, data);
                out.add(packet);
                ctx.channel().attr(ConnectionHandler.WAITING_FOR_LOGIN_KEY).set(false);
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        cause.printStackTrace();
    }
}
