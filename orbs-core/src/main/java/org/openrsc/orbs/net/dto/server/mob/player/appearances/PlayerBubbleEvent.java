package org.openrsc.orbs.net.dto.server.mob.player.appearances;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerUpdateType;

@SuperBuilder
@Getter
public class PlayerBubbleEvent extends PlayerUpdateEvent {
    private final PlayerUpdateType type = PlayerUpdateType.BUBBLE;
    private final int itemId;
}
