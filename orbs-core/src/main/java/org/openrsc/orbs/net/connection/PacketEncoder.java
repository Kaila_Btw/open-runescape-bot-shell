package org.openrsc.orbs.net.connection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@Log4j2
public class PacketEncoder extends MessageToByteEncoder<Packet> {
    @Override
    protected void encode(ChannelHandlerContext ctx, Packet message, ByteBuf out) throws Exception {
        log.debug("Sending packet " + ClientOpcode.fromOpcode(message.getID()));
        if (message.getID() == ClientOpcode.LOGIN.getOpCode()) {
            ctx.channel().attr(ConnectionHandler.WAITING_FOR_LOGIN_KEY).set(true);
        }
        if (message.isRaw()) {
            out.writeBytes(message.getBuffer());
        } else {
            // This is code only to support RSCL based clients which simplified the network protocol
            int packetLength = message.getBuffer().readableBytes();
            ByteBuf buffer = Unpooled.buffer(packetLength + 1);

            buffer.writeShort(buffer.capacity());
            buffer.writeByte(message.getID());

            buffer.writeBytes(message.getBuffer());
            out.writeBytes(buffer);
        }
    }
}
