package org.openrsc.orbs.net.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ItemLoc {
    private final boolean noted;
    private final int id;
    private final int respawnTime;
    private final int offsetX;
    private final int offsetY;
}
