package org.openrsc.orbs.net.dto.client.command;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterActionOpcode(ClientOpcode.ITEM_COMMAND)
public class ItemCommandAction extends Action {
    @TypeOverride(DataType.SHORT)
    private final int slotId;
    private final int amount;
    @TypeOverride(DataType.BYTE)
    private final int commandId;
}
