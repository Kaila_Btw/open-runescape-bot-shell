package org.openrsc.orbs.net.serializer;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.MutableClassToInstanceMap;
import org.openrsc.orbs.net.annotation.PacketSerializable;

public class SerializerUtils {
    private static final ClassToInstanceMap<PacketDeserializer<?>> deserializerMap;
    private static final ClassToInstanceMap<PacketSerializer<?>> serializerMap;

    static {
        deserializerMap = MutableClassToInstanceMap.create();
        serializerMap = MutableClassToInstanceMap.create();
    }

    @SuppressWarnings("unchecked")
    public static <T> PacketDeserializer<T> getDeserializer(Class<T> type) {
        if (type.isAnnotationPresent(PacketSerializable.class)) {
            PacketSerializable packetSerializable = type.getAnnotation(PacketSerializable.class);
            Class<PacketDeserializer<T>> deserializerClass = (Class<PacketDeserializer<T>>) packetSerializable.deserializer();
            return (PacketDeserializer<T>) deserializerMap.computeIfAbsent(
                    deserializerClass,
                    deserializerType -> {
                        try {
                            return deserializerType.getConstructor().newInstance();
                        } catch (Exception ex) {
                            throw new RuntimeException(ex);
                        }
                    }
            );
        }
        return new DefaultPacketSerializer<>();
    }

    @SuppressWarnings("unchecked")
    public static <T> PacketSerializer<T> getSerializer(Class<T> type) {
        if (type.isAnnotationPresent(PacketSerializable.class)) {
            PacketSerializable packetSerializable = type.getAnnotation(PacketSerializable.class);
            Class<PacketSerializer<T>> serializerClass = (Class<PacketSerializer<T>>) packetSerializable.serializer();
            return (PacketSerializer<T>) serializerMap.computeIfAbsent(
                    serializerClass,
                    serializerType -> {
                        try {
                            return serializerType.getConstructor().newInstance();
                        } catch (Exception ex) {
                            throw new RuntimeException(ex);
                        }
                    }
            );
        }
        return new DefaultPacketSerializer<>();
    }
}
