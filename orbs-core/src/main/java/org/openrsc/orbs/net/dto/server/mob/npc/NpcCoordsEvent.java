package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.NpcCoordsEventDeserializer;

import java.util.LinkedHashMap;
import java.util.Set;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = NpcCoordsEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_NPC_COORDS)
public class NpcCoordsEvent extends Event {
    private final Set<Integer> removedNpcs;
    private final LinkedHashMap<Integer, NpcPositionUpdate> npcStatusUpdates;
}
