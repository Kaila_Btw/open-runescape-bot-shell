package org.openrsc.orbs.net.annotation;

import org.openrsc.orbs.net.annotation.model.SerializationStrategy;
import org.openrsc.orbs.net.serializer.DefaultPacketSerializer;
import org.openrsc.orbs.net.serializer.PacketDeserializer;
import org.openrsc.orbs.net.serializer.PacketSerializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PacketSerializable {
    SerializationStrategy strategy() default SerializationStrategy.IMPLICIT;

    Class<? extends PacketSerializer> serializer() default DefaultPacketSerializer.class;

    Class<? extends PacketDeserializer> deserializer() default DefaultPacketSerializer.class;
}
