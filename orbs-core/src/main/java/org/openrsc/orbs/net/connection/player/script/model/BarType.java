package org.openrsc.orbs.net.connection.player.script.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.net.connection.player.script.ItemIds;
import org.openrsc.orbs.net.connection.player.script.params.Itemable;

@AllArgsConstructor
@Getter
public enum BarType implements Itemable {
    BRONZE("Bronze", 1, 169, ItemIds.COPPER_ORE.id(), ItemIds.TIN_ORE.id(), 1, ItemIds.BRONZE_BAR.id()),
    IRON("Iron", 15, 170, ItemIds.IRON_ORE.id(), -1, 0, ItemIds.IRON_BAR.id()),
    SILVER("Silver", 20, 384, 383, -1, 0, ItemIds.SILVER_BAR.id()),
    STEEL("Steel", 30, 171, ItemIds.IRON_ORE.id(), ItemIds.COAL.id(), 2, ItemIds.STEEL_BAR.id()),
    GOLD("Gold", 40, 172, ItemIds.GOLD.id(), null, null, ItemIds.GOLD_BAR.id()),
    MITHRIL("Mithril", 50, 173, ItemIds.MITHRIL_ORE.id(), ItemIds.COAL.id(), 4, ItemIds.MITHRIL_BAR.id()),
    ADAMANTITE("Adamantite", 70, 174, ItemIds.ADAMANTITE_ORE.id(), ItemIds.COAL.id(), 6, ItemIds.ADAMANTITE_BAR.id()),
    RUNITE("Runite", 408, 85, ItemIds.RUNITE_ORE.id(), ItemIds.COAL.id(), 8, ItemIds.RUNITE_BAR.id());

    private final String name;
    private final Integer minLevel;
    private final Integer id;
    private final Integer oreId;
    private final Integer coalId; // for bronze
    private final Integer coalCount;
    private final Integer itemId;

    @Override
    public String toString() {
        return getName();
    }
}