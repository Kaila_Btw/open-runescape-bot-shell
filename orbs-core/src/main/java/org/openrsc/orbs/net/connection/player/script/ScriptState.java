package org.openrsc.orbs.net.connection.player.script;

public enum ScriptState {
    RUNNING,
    PAUSED,
    SLEEPING,
    SHUTTING_DOWN,
    FINISHED,
    KILLED
}
