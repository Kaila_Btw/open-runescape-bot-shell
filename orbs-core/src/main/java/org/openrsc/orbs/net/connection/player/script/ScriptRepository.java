package org.openrsc.orbs.net.connection.player.script;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.springframework.stereotype.Component;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Log4j2
@Component
@SuppressWarnings("rawtypes")
public class ScriptRepository {
    private final Map<String, Class<? extends Script>> loadedScripts = new HashMap<>();

    public ScriptRepository() {
        try {
            Reflections reflections = new Reflections("org.openrsc.orbs.scripts");
            final Set<Class<? extends Script>> subTypesOf = reflections.getSubTypesOf(Script.class);
            subTypesOf.forEach((type) -> loadedScripts.put(type.getSimpleName(), type));
            log.info(loadedScripts.size() + " scripts loaded.");
        } catch (Exception ex) {
            log.error(ex);
        }
    }

    public Class<? extends Script> getClass(String name) {
        if (loadedScripts.containsKey(name)) {
            return loadedScripts.get(name);
        }
        return null;
    }

    public Set<Pair<String, Class<? extends Script>>> getScripts() {
        Set<Pair<String, Class<? extends Script>>> set = new HashSet<>();
        for (Map.Entry<String, Class<? extends Script>> entry : loadedScripts.entrySet()) {
            Pair<String, Class<? extends Script>> of = Pair.of(entry.getKey(), entry.getValue());
            set.add(of);
        }
        return set;
    }

    public Script<?> getNullAPIInstance(String name) {
        if (loadedScripts.containsKey(name)) {
            Class<? extends Script> type = getClass(name);
            ScriptAPI nullAPI = null;
            try {
                final Constructor<? extends Script> declaredConstructor = type.getDeclaredConstructor(ScriptAPI.class);
                declaredConstructor.setAccessible(true);
                return declaredConstructor.newInstance(nullAPI);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                log.error(e);
            }
        }
        return null;
    }
}
