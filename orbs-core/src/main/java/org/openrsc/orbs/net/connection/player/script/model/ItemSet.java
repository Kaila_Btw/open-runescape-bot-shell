package org.openrsc.orbs.net.connection.player.script.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;

@Builder(toBuilder = true)
@Getter
@ToString
public class ItemSet {
    @Singular
    private final Set<Item> items;

    @SafeVarargs
    public static ItemSet of(Pair<Integer, Integer>... itemIdAmounts) {
        final ItemSetBuilder builder = ItemSet.builder();
        for (Pair<Integer, Integer> idAmountPair : itemIdAmounts) {
            builder.item(
                    Item.builder()
                            .id(idAmountPair.getKey())
                            .amount(idAmountPair.getValue())
                            .build()
            );
        }
        return builder.build();
    }

    public int getTotalCost() {
        return items.stream()
                .mapToInt(Item::getAmount)
                .sum();
    }

    public ItemSet multiply(int multiplier) {
        final ItemSetBuilder builder = ItemSet.builder();
        for (Item item : items) {
            builder.item(
                    Item.builder()
                            .id(item.getId())
                            .amount(item.getAmount() * multiplier)
                            .build()
            );
        }
        return builder.build();
    }
}
