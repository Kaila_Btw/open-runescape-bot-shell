package org.openrsc.orbs.net.annotation.model;

public enum SerializationStrategy {
    IMPLICIT,
    EXPLICIT,
    CUSTOM
}
