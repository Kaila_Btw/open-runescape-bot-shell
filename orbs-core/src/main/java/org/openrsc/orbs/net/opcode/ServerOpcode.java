package org.openrsc.orbs.net.opcode;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// This is from the server's perspective. Server --> OpcodeOut
@AllArgsConstructor
@Getter
public enum ServerOpcode {
    RAW_RESPONSE(-1),
    SEND_LOGOUT_REQUEST_CONFIRM(4),
    SEND_QUESTS(5),
    SEND_DUEL_OPPONENTS_ITEMS(6),
    SEND_TRADE_ACCEPTED(15),
    SEND_SERVER_CONFIGS(19), // custom
    SEND_TRADE_OPEN_CONFIRM(20),
    SEND_WORLD_INFO(25),
    SEND_DUEL_SETTINGS(30),
    SEND_EXPERIENCE(33),
    SEND_EXPERIENCE_TOGGLE(34), // custom
    SEND_BUBBLE(36), // used for teleport, telegrab, and iban's magic
    SEND_BANK_OPEN(42),
    SEND_SCENERY_HANDLER(48),
    SEND_PRIVACY_SETTINGS(51),
    SEND_SYSTEM_UPDATE(52),
    SEND_INVENTORY(53),
    SEND_ELIXIR(54), // custom
    SEND_APPEARANCE_SCREEN(59),
    SEND_NPC_COORDS(79),
    SEND_DEATH(83),
    SEND_STOPSLEEP(84),
    SEND_PRIVATE_MESSAGE_SENT(87),
    SEND_BOX2(89),
    SEND_INVENTORY_UPDATEITEM(90),
    SEND_BOUNDARY_HANDLER(91),
    SEND_TRADE_WINDOW(92),
    SEND_TRADE_OTHER_ITEMS(97),
    SEND_EXPSHARED(98), // custom
    SEND_GROUND_ITEM_HANDLER(99),
    SEND_SHOP_OPEN(101),
    SEND_UPDATE_NPC(104),
    SEND_IGNORE_LIST(109),
    SEND_INPUT_BOX(110), // custom
    SEND_ON_TUTORIAL(111),
    SEND_CLAN(112), // custom
    SEND_CLAN_LIST(112), // custom - shares opcode currently should be changed in future
    SEND_CLAN_SETTINGS(112), // custom
    SEND_IRONMAN(113), // custom
    SEND_FATIGUE(114),
    SEND_ON_BLACK_HOLE(115), // custom
    SEND_PARTY(116), // custom
    SEND_PARTY_LIST(116), // custom - shares opcode currently should be changed in future
    SEND_PARTY_SETTINGS(116), // custom
    SEND_SLEEPSCREEN(117),
    SEND_KILL_ANNOUNCEMENT(118), // custom
    SEND_PRIVATE_MESSAGE(120),
    SEND_INVENTORY_REMOVE_ITEM(123),
    SEND_TRADE_CLOSE(128),
    SEND_COMBAT_STYLE(129), // custom
    SEND_SERVER_MESSAGE(131),
    SEND_AUCTION_PROGRESS(132), // custom
    SEND_FISHING_TRAWLER(133), // custom
    SEND_STATUS_PROGRESS_BAR(134), // custom, formerly separated labeled progress, update, remove
    SEND_BANK_PIN_INTERFACE(135), // custom
    SEND_ONLINE_LIST(136), // custom
    SEND_SHOP_CLOSE(137),
    SEND_OPENPK_POINTS_TO_GP_RATIO(144), // custom
    SEND_NPC_KILLS(147), // custom
    SEND_OPENPK_POINTS(148), // custom
    SEND_FRIEND_UPDATE(149),
    SEND_BANK_PRESET(150), // custom
    SEND_EQUIPMENT_STATS(153),
    SEND_STATS(156),
    SEND_STAT(159),
    SEND_TRADE_OTHER_ACCEPTED(162),
    SEND_LOGOUT(165),
    SEND_DUEL_CONFIRMWINDOW(172),
    SEND_DUEL_WINDOW(176),
    SEND_WELCOME_INFO(182),
    SEND_CANT_LOGOUT(183),
    SEND_28_BYTES_UNUSED(189),
    SEND_PLAYER_COORDS(191),
    SEND_SLEEPWORD_INCORRECT(194),
    SEND_BANK_CLOSE(203),
    SEND_PLAY_SOUND(204),
    SEND_PRAYERS_ACTIVE(206),
    SEND_DUEL_ACCEPTED(210),
    SEND_REMOVE_WORLD_ENTITY(211),
    SEND_APPEARANCE_KEEPALIVE(213),
    SEND_BOX(222),
    SEND_OPEN_RECOVERY(224), // part of rsc era protocol
    SEND_DUEL_CLOSE(225),
    SEND_OPEN_DETAILS(232), // part of rsc era protocol
    SEND_UPDATE_PLAYERS(234),
    SEND_UPDATE_IGNORE_LIST_BECAUSE_NAME_CHANGE(237),
    SEND_GAME_SETTINGS(240),
    SEND_SLEEP_FATIGUE(244),
    SEND_OPTIONS_MENU_OPEN(245),
    SEND_BANK_UPDATE(249),
    SEND_OPTIONS_MENU_CLOSE(252),
    SEND_DUEL_OTHER_ACCEPTED(253),
    SEND_EQUIPMENT(254), // custom
    SEND_EQUIPMENT_UPDATE(255); // custom

    private static final Map<Integer, ServerOpcode> opcodeMap;

    static {
        opcodeMap = new HashMap<>();
        Arrays.stream(ServerOpcode.values())
                .forEach(ServerOpcode::addToMap);
    }

    private final int opCode;

    private static void addToMap(ServerOpcode serverOpcode) {
        opcodeMap.put(serverOpcode.getOpCode(), serverOpcode);
    }

    public static ServerOpcode fromOpcode(int id) {
        return opcodeMap.get(id);
    }
}
