package org.openrsc.orbs.net.dto.server.trade;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterEventOpcode(ServerOpcode.SEND_TRADE_WINDOW)
@ToString
public class SendTradeWindowEvent extends Event {
    @TypeOverride(DataType.SHORT)
    private final int playerId;
}
