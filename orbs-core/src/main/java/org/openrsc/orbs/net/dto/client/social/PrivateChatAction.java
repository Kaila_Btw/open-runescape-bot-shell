package org.openrsc.orbs.net.dto.client.social;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@AllArgsConstructor
@Getter
@PacketSerializable
@RegisterActionOpcode(ClientOpcode.SOCIAL_SEND_PRIVATE_MESSAGE)
public class PrivateChatAction {
    private final String player;
    @TypeOverride(DataType.ENCRYPTED_STRING)
    private final String message;
}
