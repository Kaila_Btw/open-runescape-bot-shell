package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.NpcVisualUpdateEventDeserializer;

import java.util.List;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = NpcVisualUpdateEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_UPDATE_NPC)
public class NpcVisualUpdateEvent extends Event {
    @Singular
    private final List<NpcUpdateEvent> updateEvents;
}
