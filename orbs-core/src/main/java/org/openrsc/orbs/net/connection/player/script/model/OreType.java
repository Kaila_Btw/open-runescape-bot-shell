package org.openrsc.orbs.net.connection.player.script.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.net.connection.player.script.CommonIds;
import org.openrsc.orbs.net.connection.player.script.ItemIds;
import org.openrsc.orbs.net.connection.player.script.params.Itemable;

@AllArgsConstructor
@Getter
public enum OreType implements Itemable {
    CLAY(ItemIds.CLAY.id(), "Clay", CommonIds.OBJ_CLAY),
    TIN(ItemIds.TIN_ORE.id(), "Tin", CommonIds.OBJ_TIN),
    COPPER(ItemIds.COPPER_ORE.id(), "Copper", CommonIds.OBJ_COPPER),
    IRON(ItemIds.IRON_ORE.id(), "Iron", CommonIds.OBJ_IRON),
    SILVER(ItemIds.SILVER.id(), "Silver", CommonIds.OBJ_SILVER),
    COAL(ItemIds.COAL.id(), "Coal", CommonIds.OBJ_COAL),
    GOLD(ItemIds.GOLD.id(), "Gold", CommonIds.OBJ_GOLD),
    MITHRIL(ItemIds.MITHRIL_ORE.id(), "Mithril", CommonIds.OBJ_MITHRIL),
    ADAMANTITE(ItemIds.ADAMANTITE_ORE.id(), "Adamantite", CommonIds.OBJ_ADAMANTITE),
    RUNITE(ItemIds.RUNITE_ORE.id(), "Runite", CommonIds.OBJ_RUNITE);

    private final Integer itemId;
    private final String label;
    private final int[] objectIds;

    @Override
    public String toString() {
        return getLabel();
    }
}