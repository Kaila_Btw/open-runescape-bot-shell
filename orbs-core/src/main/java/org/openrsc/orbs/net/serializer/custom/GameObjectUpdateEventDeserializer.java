package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.net.dto.GameObjectLoc;
import org.openrsc.orbs.net.dto.server.objects.BoundaryUpdateEvent;
import org.openrsc.orbs.net.dto.server.objects.GameObjectUpdateEvent;
import org.openrsc.orbs.net.dto.server.objects.SceneryUpdateEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

import java.util.ArrayList;
import java.util.List;


public class GameObjectUpdateEventDeserializer implements PacketDeserializer<GameObjectUpdateEvent> {
    @Override
    public GameObjectUpdateEvent deserialize(Packet packet, Class<?> type) {
        GameObjectUpdateEvent.GameObjectUpdateEventBuilder builder;
        if (packet.getID() == ServerOpcode.SEND_SCENERY_HANDLER.getOpCode()) {
            builder = SceneryUpdateEvent.builder();
        } else {
            builder = BoundaryUpdateEvent.builder();
        }

        int payloadSize = 5;
        int iterations = packet.getReadableBytes() / payloadSize;
        List<GameObjectLoc> objects = new ArrayList<>();
        for (int i = 0; i < iterations; i++) {
            int id = packet.readShort();
            int offsetX = packet.readByte();
            int offsetY = packet.readByte();
            int direction = packet.readByte();
            objects.add(
                    new GameObjectLoc(direction, id, offsetX, offsetY)
            );
        }

        return builder.objects(objects).build();
    }
}
