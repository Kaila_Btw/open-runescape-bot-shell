package org.openrsc.orbs.net.connection.player.script.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.stateful.player.model.Player;

@AllArgsConstructor
@Getter
public class DuelRequest {
    private final Player playerId;
}
