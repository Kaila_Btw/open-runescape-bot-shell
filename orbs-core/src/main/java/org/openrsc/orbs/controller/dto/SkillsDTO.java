package org.openrsc.orbs.controller.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

import java.util.Set;

@Builder(toBuilder = true)
@Getter
public class SkillsDTO {
    @Singular
    private final Set<LabeledNumberDTO> skills;
}
