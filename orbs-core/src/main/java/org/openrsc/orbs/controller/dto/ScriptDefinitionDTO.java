package org.openrsc.orbs.controller.dto;

import lombok.Builder;
import lombok.Getter;
import org.openrsc.orbs.net.connection.player.script.params.ScriptParam;

import java.util.List;

@Builder(toBuilder = true)
@Getter
public class ScriptDefinitionDTO<T> {
    private final String name;
    private final T defaultParameters;
    private final List<ScriptParam> options;
}
